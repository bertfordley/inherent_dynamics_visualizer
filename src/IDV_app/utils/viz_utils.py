import os
import matplotlib
import dash_table
import pandas as pd
from scipy import stats
import utils.utils as utils
import plotly.express as px
import plotly.graph_objs as go
import dash_core_components as dcc
import dash_html_components as html
import dsgrn_utilities.network_distance as netdist


#make colormap haase lab color scheme
norm = matplotlib.colors.Normalize(-1.5,1.5)
colors = [[norm(-1.5), "cyan"],
          [norm(0), "black"],
         [norm(1.5), "yellow"]]
haase = matplotlib.colors.LinearSegmentedColormap.from_list("", colors)


def heatmap_max_node(data, title, gene_list, first_period, width, height):
    # data = data frame with gene names as the index
    # gene list = list of gene names to plot
    # first period = integer value of the last column of the first period

    # yticks: default False doesn't plot ytick labels
    # cbar_bool: default True does include color bar
    # axis: default None doesn't specify an axis
    # data = data.loc[gene_list]
    data = data.reindex(index=gene_list)
    z_pyjtk = stats.zscore(data, axis=1)
    z_pyjtk_df = pd.DataFrame(z_pyjtk, index=data.index, columns=data.columns)
    z_pyjtk_1stperiod = z_pyjtk_df.iloc[:, 0:first_period]
    max_time = z_pyjtk_1stperiod.idxmax(axis=1)
    z_pyjtk_df["max"] = max_time
    z_pyjtk_df["max"] = pd.to_numeric(z_pyjtk_df["max"])
    z_pyjtk_df = z_pyjtk_df.sort_values(by="max", axis=0)
    z_pyjtk_df = z_pyjtk_df.drop(columns=['max'])
    heat = px.imshow(z_pyjtk_df, width=width, height=height, zmin=-1.5, zmax=1.5,
                     labels={'x': 'Time', 'y': ''}, color_continuous_scale=colors, aspect=100)
    # heat = px.imshow(z_pyjtk_df, zmin=-1.5, zmax=1.5,
    #                 labels={'x': 'Time', 'y': ''}, color_continuous_scale=colors)
    # heat.update_yaxes(showticklabels=False)
    # heat.update_layout(margin=dict(t=40, b=20))
    heat.update_layout(title_text=title, title_x=0.5, title_font_size=12,
                       legend_yanchor='top')
    heat.update_xaxes(tickangle=90)
    return heat


## function to plot data in a heat map, sorted based off of the supplied order, and normalized by z-score
def heatmap_order_node(data, title, gene_list, width, height):
    # data = data frame with gene names as the index
    #order = list of genes to plot in desired order
    # yticks: default False doesn't plot ytick labels
    # cbar_bool: default True does include color bar
    # axis: default None doesn't specify an axis
    # data = data.loc[order]
    data = data.reindex(gene_list)
    z_pyjtk = stats.zscore(data, axis=1)
    z_pyjtk_df = pd.DataFrame(z_pyjtk, index=data.index, columns=data.columns)
    heat = px.imshow(z_pyjtk_df, width=width, height=height, zmin=-1.5, zmax=1.5,
                     labels={'x': 'Time', 'y': ''}, color_continuous_scale=colors, aspect=100)
    # heat.update_yaxes(showticklabels=False)
    # heat.update_layout(margin=dict(t=40, b=20))
    heat.update_layout(title_text=title, title_x=0.5, title_font_size=12,
                       legend_yanchor='top')
    heat.update_xaxes(tickangle=90)
    return heat


def lem_scatter(df, xaxis, graph_title):

    return px.histogram(df, x=xaxis, title=graph_title)


def dsgrn_scatter_summary(df, xaxis, yaxis, epilson, title):

    epilson_df = df[df['Epsilon'] == epilson]
    scatter_fig = px.scatter(epilson_df, x=xaxis, y=yaxis,
                             width=600, height=300,
                             hover_data=['Parameter Graph Size'],
                             custom_data=['Network Toplogy'])
    scatter_fig.update_layout(title_text=title, title_x=0.5, title_font_size=12,
                       legend_yanchor='top')
    scatter_fig.update_layout(clickmode='event')
    # scatter_fig.update_layout({
    #     'paper_bgcolor': 'rgba(0,0,0,0)',
    #     'plot_bgcolor': 'rgba(0,0,0,0)'})
    return scatter_fig


def dsgrn_scatter_epsilons(df, xaxis, yaxis, epilson, option, title, selectedpoints):
    
    epilson_df = df[df['Epsilon'] == epilson]

    if option == 'stablefc':
        custom_data_cols = ['Network Toplogy', 'Epsilon', '% Matches', '% Stable Full Cycle', 'Network Index', 'Epsilon Network Index']
    elif option == 'domain':
        custom_data_cols = ['Network Toplogy', 'Epsilon', '% Matches', 'Matches', 'Network Index', 'Epsilon Network Index']
    
    scatter_fig = px.scatter(epilson_df, x=xaxis, y=yaxis,
                             width=700, height=500,
                             custom_data=custom_data_cols)

    scatter_fig.update_layout(title_text=title, title_x=0.5, title_font_size=12, legend_yanchor='top')
    scatter_fig.update_layout(dragmode='select', hovermode=False)

    if selectedpoints is not None:

        scatter_fig.update_traces(selectedpoints=selectedpoints,
                                  mode='markers+text',
                                  unselected={'marker': {'opacity': 0.3, 'size': 4, 'color': 'red'}}
                                  )
    else:
        scatter_fig.update_traces(selectedpoints=[],
                                  mode='markers+text',
                                  unselected={'marker': {'opacity': 0.3, 'size': 4, 'color': 'red'}}
                                  )

    return scatter_fig


def node_finding_line_graph(df, title, value, width, height, bin_edges):

    df.index.name = 'Gene'
    df = df.reset_index()
    df = df.melt(id_vars='Gene', var_name='Time', value_name=value)
    df["Time"] = pd.to_numeric(df["Time"])
    df["z-score"] = pd.to_numeric(df["z-score"])
    max_y, min_y = max(df['z-score']), min(df['z-score'])
    line_fig = px.line(df, x='Time', y=value, color='Gene', width=width, height=height)
    for idx, b in enumerate(bin_edges):
        if idx+1 < len(bin_edges) and b > min(df["Time"]):
            line_fig.add_shape(type='line', line=dict(dash="dash", width=1),
                               xref="x", x0=int(b), x1=int(b),
                               yref="y", y0=min_y, y1=max_y + 0.3)
        elif idx+1 == len(bin_edges):
            line_fig.add_shape(type='line',
                               xref="x", x0=int(b), x1=int(b),
                               yref="y", y0=min_y, y1=max_y + 0.3)
        if idx+1 < len(bin_edges):
            mid_bin = bin_edges[1] - (bin_edges[1] / 2)
            line_fig.add_annotation(x=b + mid_bin, y=max_y + 0.3,
                                    text=str(idx+1),
                                    showarrow=False)
    line_fig.add_annotation(x=bin_edges[-1]/2, y=max_y + 0.7,
                            text='Bins',
                            showarrow=False)
    line_fig.add_annotation(x=int(bin_edges[-1]), y=max_y + 0.7,
                       text="1st Cycle",
                       showarrow=False)
    line_fig.update_layout(title_text=title, title_x=0.5, title_font_size=12, title_yanchor='top')
    line_fig.update_xaxes(tickangle=90)
    line_fig.update_layout({
        'plot_bgcolor': 'rgba(0,0,0,0)'})
    line_fig.update_xaxes(showline=True, linewidth=0.5, linecolor='black')
    line_fig.update_yaxes(showline=True, linewidth=0.5, linecolor='black')
    # line_fig.update_layout({
    #     'paper_bgcolor': 'rgba(0,0,0,0)',
    #     'plot_bgcolor': 'rgba(0,0,0,0)'})

    return line_fig


def get_seed_net_args_table_netf(network_finding_config):

    range_ops = f'{network_finding_config["range_operations"][0]} - {network_finding_config["range_operations"][1]}'

    seed_net_args = [{'Argument': 'Range of Operations', 'Setting': range_ops},
                     {'Argument': '# of Neighbors', 'Setting': network_finding_config['numneighbors']},
                     {'Argument': 'Max. Parameters', 'Setting': network_finding_config['maxparams']},
                     {'Argument': 'Add Node Probability', 'Setting': network_finding_config['probabilities']['addNode']},
                     {'Argument': 'Add Edge Probability', 'Setting': network_finding_config['probabilities']['addEdge']},
                     {'Argument': 'Remove Node Probability', 'Setting': network_finding_config['probabilities']['removeNode']},
                     {'Argument': 'Remove Edge Probability', 'Setting': network_finding_config['probabilities']['removeEdge']}]
    seed_net_arg_df = pd.DataFrame.from_records(seed_net_args)
    seed_args_table = dash_table.DataTable(
        id='seed_net_config_netf',
        columns=[{'name': i, 'id': i} for i in seed_net_arg_df.columns],
        data=seed_net_arg_df.to_dict('records'),
        fill_width=False,
        style_cell={'textAlign': 'left',
                    'whiteSpace': 'normal',
                    'height': 'auto',
                    },
        style_data_conditional=[
            {
                'if': {'row_index': 'odd'},
                'backgroundColor': 'rgb(248, 248, 248)'
            }
        ],
        style_header={
            'backgroundColor': 'rgb(230, 230, 230)',
            'fontWeight': 'bold'
        }
    )

    return seed_args_table


def make_single_net_table_netf(single_net_df, option, index):

    if option == 'stablefc':
        cols = ['Epsilon', '% Matches', '% Stable Full Cycle', 'Parameter Graph Size']
    elif option == 'domain':
        cols = ['Epsilon', '% Matches', 'Matches', 'Parameter Graph Size']

    table_data = single_net_df.drop(columns=[c for c in single_net_df.columns if c not in cols])
    table_data = table_data.sort_values(by="Epsilon")

    single_net_table = dash_table.DataTable(
        id={'type': 'single_net_datatable', 'index': f'single_net_{index}'},
        columns=[{'name': i, 'id': i} for i in table_data.columns],
        data=table_data.to_dict('records'),
        fill_width=False,
        style_cell={'textAlign': 'left',
                    'whiteSpace': 'normal',
                    'height': 'auto',
                    },
        style_data_conditional=[
            {
                'if': {'row_index': 'odd'},
                'backgroundColor': 'rgb(248, 248, 248)'
            }
        ],
        style_header={
            'backgroundColor': 'rgb(230, 230, 230)',
            'fontWeight': 'bold'
        })

    return single_net_table

def make_single_net_table_sim_netf(single_net_df, option, index):
    if option == 'SFC':
        cols = ['Epsilon', '% Matches', '% Stable Full Cycle', 'Parameter Graph Size']
    elif option == 'matches':
        cols = ['Epsilon', '% Matches', '% Stable Full Cycle', 'Parameter Graph Size']
    elif option == 'domain':
        cols = ['Epsilon', '% Matches', 'Matches', 'Parameter Graph Size']
    table_data = single_net_df.drop(columns=[c for c in single_net_df.columns if c not in cols])
    single_net_table = dash_table.DataTable(
        id={'type': 'single_net_datatable', 'index': f'single_net_{index}'},
        columns=[{'name': i, 'id': i} for i in table_data.columns],
        data=table_data.to_dict('records'),
        fill_width=False,
        style_cell={'textAlign': 'left',
                    'whiteSpace': 'normal',
                    'height': 'auto',
                    },
        style_data_conditional=[
            {
                'if': {'row_index': 'odd'},
                'backgroundColor': 'rgb(248, 248, 248)'
            }
        ],
        style_header={
            'backgroundColor': 'rgb(230, 230, 230)',
            'fontWeight': 'bold'
        })
    return single_net_table


def make_manifest_table():

    dataset_arg_dict = utils.parse_dataset_arg()

    manifest = dataset_arg_dict['manifest'].rename(columns={'run_step': 'IDP Algorithm', 'run_input': 'IDP Input', 'run_output': 'IDP Output'})

    step_algo_dict = {'dlxjtk': 'Node Finding',
                      'lempy': 'Edge Finding',
                      'netgen': 'Edge Finding',
                      'netper': 'Network Finding',
                      'netquery': 'Network Finding'}

    manifest['IDP Step'] = manifest['IDP Algorithm'].apply(lambda alg: step_algo_dict[alg])
    step_column = manifest.pop('IDP Step')
    manifest.insert(0, 'IDP Step', step_column)
    manifest['IDP Input'] = manifest['IDP Input'].str.replace('<br>',', ')

    updated_output = list()
    empty_dirs = dataset_arg_dict['empty_dirs']

    for folder in manifest['IDP Output'].tolist():
        if 'networks' in folder:
            new_name = f'network_finding_{folder.split("networks")[1]}'
            if new_name in empty_dirs:
                updated_output.append(f'{folder} (No Results)')
            else:
                updated_output.append(folder)
        elif 'queries' in folder:
            new_name = f'network_queries_{folder.split("queries")[1]}'
            if new_name in empty_dirs:
                updated_output.append(f'{folder} (No Results)')
            else:
                updated_output.append(folder)
        else:
            if folder in empty_dirs:
                updated_output.append(f'{folder} (No Results)')
            else:
                updated_output.append(folder)

    manifest['IDP Output'] = updated_output

    mani_table = dash_table.DataTable(
        id='mani_table',
        columns=[{"name": i, "id": i} for i in manifest.columns],
        data=manifest.to_dict('records'),
        style_cell={'textAlign': 'center', 
        'border': '1px solid grey',
        'height': '50px'},
        style_header={ 'border': '1px solid black'},
    )

    return mani_table


def make_results_tree():
    dataset_arg_dict = utils.parse_dataset_arg()
    step_results_dirs = dataset_arg_dict['manifest'].values.tolist()

    step_pos_dict = {'dlxjtk': [0, 1],
                     'lempy': [1, 2],
                     'netgen': [2, 3],
                     'netper': [3, 4],
                     'netquery': [4, 5]}

    step_count_dict = {'dlxjtk': [],
                       'lempy': [],
                       'netgen': [],
                       'netper': [],
                       'netquery': []}

    results_dirs = [item for sublist in step_results_dirs for item in sublist[1:]]
    dir_seen = set()
    dir_seen_add = dir_seen.add
    v_label = [rdir for rdir in results_dirs if not (rdir in dir_seen or dir_seen_add(rdir))]
    nr_vertices = len(v_label)

    edge_seq = list()
    position = dict()

    for step_dirs in step_results_dirs:
        step = step_dirs[0]
        rdirs = step_dirs[1:]
        dir1, dir2 = rdirs
        if v_label.index(dir1) not in position.keys():
            position[v_label.index(dir1)] = [len(step_count_dict[step])/10, float(round(step_pos_dict[step][rdirs.index(rdirs[0])], 1))]
        if v_label.index(dir2) not in position.keys():
            position[v_label.index(dir2)] = [len(step_count_dict[step])/10, float(round(step_pos_dict[step][rdirs.index(rdirs[1])], 1))]
        step_count_dict[step].append(dir1)
        step_count_dict[step].append(dir2)

        edge_seq.append((v_label.index(dir1), v_label.index(dir2)))

    M = len(v_label)
    L = len(position)
    Xn = [position[k][0] for k in range(L)]
    Yn = [2 * M - position[k][1] for k in range(L)]
    Xe = []
    Ye = []
    for edge in edge_seq:
        Xe += [position[edge[0]][0], position[edge[1]][0], None]
        Ye += [2 * M - position[edge[0]][1], 2 * M - position[edge[1]][1], None]

    labels = v_label

    def make_annotations(pos, text, font_size=14, font_color='rgb(0,0,0)'):
        L = len(pos)
        if len(text) != L:
            raise ValueError('The lists pos and text must have the same len')
        annotations = []
        for k in range(L):
            annotations.append(
                dict(
                    text=labels[k],  # or replace labels with a different list for the text within the circle
                    x=pos[k][0], y=2 * M - position[k][1],
                    xref='x1', yref='y1',
                    font=dict(color=font_color, size=font_size),
                    showarrow=False)
            )
        return annotations

    fig = go.Figure()

    fig.add_trace(go.Scatter(x=Xe,
                             y=Ye,
                             mode='lines',
                             line=dict(color='rgb(210,210,210)', width=2),
                             hoverinfo='none'
                             ))

    fig.add_trace(go.Scatter(x=Xn,
                             y=Yn,
                             mode='markers',
                             name='bla',
                             marker=dict(symbol='circle',
                                         size=1,
                                         color='#6175c1',  # '#DB4551',
                                         line=dict(color='rgb(50,50,50)', width=1)
                                         ),
                             text=labels,
                             hoverinfo='text',
                             opacity=0.8
                             ))
    axis = dict(showline=False,  # hide axis line, grid, ticklabels and  title
                zeroline=False,
                showgrid=False,
                showticklabels=False,
                )

    fig.update_layout(annotations=make_annotations(position, v_label),
                      font_size=16,
                      showlegend=False,
                      xaxis=axis,
                      yaxis=axis,
                      margin=dict(l=40, r=40, b=85, t=30),
                      hovermode='closest',
                      plot_bgcolor='rgb(255, 255, 255)'
                      )

    return fig


def make_lem_parameter_sliders():

    return html.Div([
        dcc.Markdown("##### Parameters:"),
        html.Div(id='param0_label_child'),
        dcc.RangeSlider(
        id='param_0_range',
        min=0,
        max=0.3,
        step=0.003,
        marks ={0:'0', 0.05:'0.05', 0.1:'0.1',0.15:'0.15',0.2:'0.2',0.25:'0.25', 0.3:'0.3'},
        value=[0, 0.3]
            ),
        html.Div(id='param1_label_child'),
        dcc.RangeSlider(
        id='param_1_range',
        min=0,
        max=1,
        step=0.01,
        marks ={0:'0', 0.1:'0.1', 0.2:'0.2', 0.3:'0.3', 0.4:'0.4', 0.5:'0.5', 0.6:'0.6', 0.7:'0.7', 0.8:'0.8', 0.9:'0.9', 1:'1'},
        value=[0, 1]
            ),
        html.Div(id='param2_label_child'),
        dcc.RangeSlider(
        id='param_2_range',
        min=0,
        max=100,
        step=1,
        marks ={0:'0', 10:'10', 20:'20', 30:'30', 40:'40', 50:'50', 60:'60', 70:'70', 80:'80', 90:'90', 100:'100'},
        value=[0, 100]
            ),
        html.Div(id='param3_label_child'),
        dcc.RangeSlider(
        id='param_3_range',
        min=0,
        max=100,
        step=1,
        marks ={0:'0', 10:'10', 20:'20', 30:'30', 40:'40', 50:'50', 60:'60', 70:'70', 80:'80', 90:'90', 100:'100'},
        value=[0, 100]),
        html.Div(id='param4_label_child'),
        dcc.RangeSlider(
        id='param_4_range',
        min=0,
        max=10,
        step=0.1,
        marks ={0:'0', 1:'1', 2:'2', 3:'3', 4:'4', 5:'5', 6:'6', 7:'7', 8:'8', 9:'9', 10:'10'},
        value=[0, 10]
            )])

def make_edge_prev_table(id):

    return dash_table.DataTable(
        id=id,
        fill_width=False,
        page_action='native',
        page_current= 0,
        page_size= 20,
        style_table={'height': '650px'},
        style_cell={'textAlign': 'left',
                    'whiteSpace': 'normal',
                    'height': 'auto',
                    },
        style_data_conditional=[{
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'}],
        style_header={
                    'backgroundColor': 'rgb(230, 230, 230)',
                    'fontWeight': 'bold'})


def make_edge_list_table(id):

    return dash_table.DataTable(
        id=id,
        sort_action="native",
        sort_mode="multi",
        row_selectable="multi",
        selected_rows=[],
        fill_width=False,
        page_action='native',
        page_current= 0,
        page_size= 20,
        style_table={'height': '650px'},
        style_cell={'textAlign': 'left',
                    'whiteSpace': 'normal',
                    'height': 'auto',
                    },
        style_data_conditional=[{
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'}],
        style_header={
                    'backgroundColor': 'rgb(230, 230, 230)',
                    'fontWeight': 'bold'})


def make_lem_summary_table(id):

    return dash_table.DataTable(
                            id=id,
                            sort_action="native",
                            sort_mode="multi",
                            row_selectable="multi",
                            selected_rows=[],
                            fill_width=False,
                            tooltip_header={
                                            'model': ['string specifying the model of regulation'],
                                            'pld': ['computed posterior probability of the model of regulation'],
                                            'loss': ['value of the loss function at the global optimum'],
                                            'norm_loss': ['loss of the model divided by the loss achieved by the `null_model`'],
                                            'prior': [' assumed prior probability of the model of regulation'],
                                            'rhs_param_0': ['γ: basal expression rate'],
                                            'rhs_param_1': ['β: degradation rate'],
                                            'rhs_param_2': ['α: maximal transcription rate'],
                                            'rhs_param_3': ['K: threshold of regulator'],
                                            'rhs_param_4': ['n: Hill coefficient']},
                            style_cell={'textAlign': 'left',
                                        'whiteSpace': 'normal',
                                        'height': 'auto',
                                        },
                            style_data_conditional=[
                                {
                                    'if': {'row_index': 'odd'},
                                    'backgroundColor': 'rgb(248, 248, 248)'
                                }
                            ],
                            style_header={
                                'backgroundColor': 'rgb(230, 230, 230)',
                                'fontWeight': 'bold'
                            },
                            page_action='none',
                            style_table={'height': '700px', 'overflowY': 'auto'}
                        )


def make_similarity_scatter(results_dir, config_file, netque_filename, option, ep, snetwork):

    if option == 'SFC' or option == 'matches':
        results, _ = utils.get_netque_results(
            f'{results_dir}/{os.path.basename(netque_filename)}',
            config_file['stablefc'], config_file['domain'])
        results_df = utils.convert_stablefc_json_to_df(results)
        if option == 'SFC':
            xaxis = "% Stable Full Cycle"
        else:
            xaxis = "% Matches"
        
    elif option == 'domain':
        _, results = utils.get_netque_results(
            f'{results_dir}/{os.path.basename(netque_filename)}',
            config_file['stablefc'], config_file['domain'])
        results_df = utils.convert_domain_json_to_df(results)
        xaxis = "Matches"

    results_df = results_df[results_df['Epsilon'] == ep]
    results_df["Similarity_to_motif"] = results_df.apply(lambda row: 1 - netdist.symmetric_difference_graph_distance(snetwork, row['Network Toplogy'], normalized=True), axis = 1)
    scatter_fig = px.scatter(results_df, x=xaxis, y="Similarity_to_motif", width=500, height=500, custom_data=['Network Toplogy', 'Network Index'])
    scatter_fig.update_layout(dragmode='select', hovermode=False)

    return scatter_fig