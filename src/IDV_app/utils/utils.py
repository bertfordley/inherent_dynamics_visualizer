import os
import re
import ast
import json
import argparse
import numpy as np
import pandas as pd
from scipy import stats
from configobj import ConfigObj

import networkx as nx
import dsgrn_utilities as du



def parse_dataset_arg():
    parser = argparse.ArgumentParser(prog='Inherent Dynamics Visualizer',
                                     description='')
    parser.add_argument('results_dir',
                        action='store',
                        help='name of results directory in pipeline_results directory')
    args = parser.parse_args()
    results_dir = args.results_dir
    subdirs_dict, tabdirs_list = get_pipeline_steps(results_dir)
    empty_dirs = check_for_empty_dirs(results_dir)
    manifest = grab_manifest(results_dir)

    dataset_arg_dict = {'results_dir': results_dir,
                        'subdirs_dict': subdirs_dict,
                        'tabdirs_list': tabdirs_list,
                        'manifest': manifest,
                        'empty_dirs': empty_dirs}

    return dataset_arg_dict


def check_for_empty_dirs(data_results_dir):

    empty_dirs = list()

    dir_contents = [pipe_dir for pipe_dir in
                    os.listdir(f'{data_results_dir}') if
                    os.path.isdir(f'{data_results_dir}/{pipe_dir}')]
    for single_dir in dir_contents:
        if len(os.listdir(os.path.join(data_results_dir, single_dir))) == 0:
            empty_dirs.append(single_dir)
    
    return empty_dirs


def grab_manifest(data_results_dir):

    def mani_path_strip(mani_row):
        path_stripped_row = list()
        path_stripped_row.append(mani_row[0])
        if mani_row[0] == 'dlxjtk':
            # Converting string to list
            filepath_list = mani_row[1].strip('][').split(', ')
            file_list = [os.path.split(fp)[1].strip("'") for fp in filepath_list]
            path_stripped_row.append('<br>'.join(file_list))
            path_stripped_row.append(os.path.split(mani_row[2])[1])
        if mani_row[0] == 'lempy':
            path_stripped_row.append(os.path.split(mani_row[1])[1])
            path_stripped_row.append(os.path.split(os.path.split(mani_row[2])[0])[1])
        if mani_row[0] == 'netgen':
            path_stripped_row.append(os.path.split(os.path.split(mani_row[1])[0])[1])
            path_stripped_row.append(os.path.split(mani_row[2])[1])
        if mani_row[0] == 'netper' or mani_row[0] == 'netquery':
            path_stripped_row.append(os.path.split(mani_row[1])[1])
            path_stripped_row.append(os.path.split(mani_row[2])[1])
        return path_stripped_row

    mani = pd.read_csv(f'{data_results_dir}/run_manifest.tsv', sep='\t', comment='#')
    short_mani = mani.apply(mani_path_strip, axis=1, result_type='expand')
    short_mani.columns = mani.columns
    return short_mani


def get_pipeline_steps(data_results_dir):

    sub_topdirs_dict = dict()
    sub_tabdirs_list = list()

    dir_contents = [pipe_dir for pipe_dir in
                    os.listdir(f'{data_results_dir}') if
                    os.path.isdir(f'{data_results_dir}/{pipe_dir}')]

    for internal_dir in dir_contents:
        if 'node_finding' in internal_dir:
            sub_tabdirs_list.append('1node_finding')
            if 'node_finding' not in sub_topdirs_dict.keys():
                sub_topdirs_dict['node_finding'] = [internal_dir]
                # sub_topdirs_dict['node_finding'].append(internal_dir)
            else:
                sub_topdirs_dict['node_finding'].append(internal_dir)
        if 'edge_finding' in internal_dir:
            sub_tabdirs_list.append('2edge_finding')
            if 'edge_finding' not in sub_topdirs_dict.keys():
                sub_topdirs_dict['edge_finding'] = [internal_dir]
            else:
                sub_topdirs_dict['edge_finding'].append(internal_dir)
        if 'seed_network' in internal_dir:
            if 'seed_network' not in sub_topdirs_dict.keys():
                sub_topdirs_dict['seed_network'] = [internal_dir]
            else:
                sub_topdirs_dict['seed_network'].append(internal_dir)
        if 'network_finding' in internal_dir:
            sub_tabdirs_list.append('3network_finding')
            if 'network_finding' not in sub_topdirs_dict.keys():
                sub_topdirs_dict['network_finding'] = [internal_dir]
            else:
                sub_topdirs_dict['network_finding'].append(internal_dir)
        if 'network_queries' in internal_dir:
            if 'network_queries' not in sub_topdirs_dict.keys():
                sub_topdirs_dict['network_queries'] = [internal_dir]
            else:
                sub_topdirs_dict['network_queries'].append(internal_dir)

    sub_tabdirs_list = list(set(sub_tabdirs_list))
    sub_tabdirs_list.sort()
    sub_tabdirs_list = [s[1:] for s in sub_tabdirs_list]

    return sub_topdirs_dict, sub_tabdirs_list


def get_dropdown_items(data_results_dir, page_name):

    page_dirs_list = list()

    dir_contents = [pipe_dir for pipe_dir in os.listdir(f'{data_results_dir}') if
        os.path.isdir(f'{data_results_dir}/{pipe_dir}')]

    empty_dirs = check_for_empty_dirs(data_results_dir)

    for contents in dir_contents:
        if page_name in contents:
            if not empty_dirs:
                page_dirs_list.append({'label': contents, 'value': contents})
            else:
                for empty in empty_dirs:
                    step, date = empty.split('_')[0], empty.split('_')[2]
                    if step in contents and date in contents:
                        pass
                    else:
                        page_dirs_list.append({'label': contents, 'value': contents})

    return page_dirs_list


def return_period_col_idx(periods, timepoints):

    avg_period = int(sum(periods)/len(periods))
    tp_ints = [int(tp) for tp in timepoints]
    clostest_timepoint = min(tp_ints, key=lambda tp: abs(tp - avg_period))
    timepoint_col_idx = tp_ints.index(clostest_timepoint)

    return avg_period, timepoint_col_idx


def pyjtk_filter_ts(ts_df, pyjtk_df, threshold):
    # filters pyjtk df on p-value column, or if thres_rank == True, filter by top N genes
    pyjtk_sorted = pyjtk_df.sort_values(by="p-value")
    pyjtk_sorted_thres = pyjtk_sorted[pyjtk_sorted["p-value"] <= threshold]
    ts_filtered_df = ts_df[ts_df.index.isin(pyjtk_sorted_thres.index)]
    return ts_filtered_df


def get_node_list(gene_list_path):
    with open(gene_list_path, 'r') as f:
        node_list = []
        for line in f:
            if not line.startswith('#'):
                node_list.append(line.strip())
    return node_list[1:]


def get_seed_file(dir_list, step_name):
    for pipeline_dir in os.listdir(dir_list):
        if step_name in pipeline_dir:
            return pipeline_dir


# parse config file
def parse_co(section,key):
    try:
        results = ast.literal_eval(section[key])
    except:
        results = section[key]
    return results


def find_pipeline_step_config(dir_list):
    for (dirpath, dirnames, filenames) in os.walk(dir_list):
        if filenames is not None:
            for filename in filenames:
                if filename.endswith('config.txt') or filename.endswith('config.tsv'):
                    return [os.path.join(dirpath, filename), False]
                if filename.endswith('config.json') or filename.endswith('_copy.json'):
                    return [os.path.join(dirpath, filename), True]


def get_pipeline_step_config(results_dir, pipeline_dir):

    config_file_path, file_is_json = find_pipeline_step_config(f'{results_dir}/{pipeline_dir}')
    # config_file_name = os.path.basename(config_file)
    if file_is_json:
        with open(config_file_path) as f:
            co = json.load(f)
    else:
        co = ConfigObj(ConfigObj(config_file_path).walk(parse_co))
    return co


def find_file(dir_path, query_filename):
    for (dirpath, dirnames, filenames) in os.walk(dir_path):
        if filenames is not None:
            for filename in filenames:
                if filename == query_filename:
                    return f'{dirpath}/{filename}'


def parse_periods(config_periods):
    # in the case that only multiple periods were specified
    if isinstance(config_periods, list):
        # check if multiple lists were provided.
        # This occurs if there is more than one dataset, which requires multiple list of periods
        if any([i.endswith(']') for i in config_periods]):
            sublist_end_bracket_idx = [i for i, s in enumerate(config_periods) if ']' in s]
            ll_periods = []
            for end_idx in range(len(sublist_end_bracket_idx)):
                if end_idx == 0:
                    sl = config_periods[:sublist_end_bracket_idx[end_idx]+1]
                else:
                    start = sublist_end_bracket_idx[end_idx-1]+1
                    sl = config_periods[start:sublist_end_bracket_idx[end_idx]+1]
                sl = [s.strip('[]') for s in sl]
                ll_periods.append(', '.join(sl))
            return ll_periods
        else:
            return '"' + ', '.join(config_periods) + '"'

    # in the case that only one period was specified
    if isinstance(config_periods, str):
        return [config_periods]


def get_netque_results(resultsdir, stablefc, domain):
    stablefc_json = False
    domain_json = False
    if stablefc:
        for quedirs in os.listdir(resultsdir):
            if quedirs.startswith('dsgrn_net_query_results'):
                for sub_quedirs in os.listdir(f'{resultsdir}/{quedirs}'):
                    if sub_quedirs.startswith('queries'):
                        for file in os.listdir(f'{resultsdir}/{quedirs}/{sub_quedirs}'):
                            if file.endswith('stablefc_all.json'):
                                with open(f'{resultsdir}/{quedirs}/{sub_quedirs}/{file}') as f:
                                    stablefc_json = json.load(f)
    if domain:
        for quedirs in os.listdir(resultsdir):
            if quedirs.startswith('dsgrn_net_query_results'):
                for sub_quedirs in os.listdir(f'{resultsdir}/{quedirs}'):
                    if sub_quedirs.startswith('queries'):
                        for file in os.listdir(f'{resultsdir}/{quedirs}/{sub_quedirs}'):
                            if file.endswith('domain_all.json'):
                                with open(f'{resultsdir}/{quedirs}/{sub_quedirs}/{file}') as f:
                                    domain_json = json.load(f)

    return stablefc_json, domain_json

def find_seed_network_edge_models_from_file(seed_net_filepath):
    edge_model_list =[]
    with open(seed_net_filepath, 'r') as f:
        for line in f:
            if len(line.split(' : ')) == 3:
                source, target, _ = line.split(' : ')
                target = target.strip('()')
            if target:
                if '~' in target:
                    t = target.strip('~')
                    model = str(source+"=tf_rep("+t+")")
                else:
                    model = str(source+"=tf_act("+target+")")
                edge_model_list.append(model)
    return edge_model_list


def get_number_of_rows_nodes(filename_list):

    if len(filename_list) % 3 > 0:
        numb_rows = int((len(filename_list) - (len(filename_list) % 3)) / 3) + 1
    else:
        numb_rows = int((len(filename_list)) / 3)
    return numb_rows


def convert_stablefc_json_to_df(stablefc_json):

    query_list_results = []
    for net, query_results in stablefc_json.items():
        for results in query_results:
            if results[2]!=0:
                row_info = [net, results[0], round(results[1] / results[2], 3), round(results[2] / results[3], 3), results[3]]
            else:
                row_info = [net, results[0], round(0, 3), round(results[2] / results[3], 3), results[3]]
            query_list_results.append(row_info)
    stablefc_df = pd.DataFrame(query_list_results, columns=['Network Toplogy', 'Epsilon', '% Matches', '% Stable Full Cycle',
                                                   'Parameter Graph Size'])
    stablefc_df = stablefc_df.reset_index()
    stablefc_df = stablefc_df.rename(columns={'index': 'Network Index'})
    ep_df_list = list()
    for ep in set(stablefc_df['Epsilon']):
        ep_df = stablefc_df[stablefc_df['Epsilon'] == ep]
        ep_df = ep_df.reset_index().reset_index()
        ep_df = ep_df.rename(columns={'level_0': 'Epsilon Network Index'})
        ep_df = ep_df.drop(['index'], axis=1)
        ep_df_list.append(ep_df)

    return pd.concat(ep_df_list)


def convert_domain_json_to_df(domain_json):

    query_list_results = []
    for net, query_results in domain_json.items():
        for results in query_results:
            row_info = [net, results[0], round(results[1] / results[2], 3), results[1], results[2]]
            query_list_results.append(row_info)
    domain_df = pd.DataFrame(query_list_results, columns=['Network Toplogy', 'Epsilon', '% Matches',
                                                          'Matches', 'Parameter Graph Size'])
    domain_df = domain_df.reset_index()
    domain_df = domain_df.rename(columns={'index': 'Network Index'})
    ep_df_list = list()
    for ep in set(domain_df['Epsilon']):
        ep_df = domain_df[domain_df['Epsilon'] == ep]
        ep_df = ep_df.reset_index().reset_index()
        ep_df = ep_df.rename(columns={'level_0': 'Epsilon Network Index'})
        ep_df = ep_df.drop(['index'], axis=1)
        ep_df_list.append(ep_df)

    return pd.concat(ep_df_list)


def netspec2CytoElems(netspec, file=False):
    '''
    Converts a DSGRN network specification into an elements list for cytoscape drawing.
    :param netspec: DSGRN network specification
    :param file: set to True if passing in a file containing one DSGRN network specification
    :return: elements list and styles sheet.
    '''
    if file:
        netspec = ''.join(open(netspec, "r").readlines())
    netnetx = du.graphtranslation.netspec2nxgraph(netspec)
    labels = nx.get_node_attributes(netnetx, "label")
    cy = nx.readwrite.json_graph.cytoscape_data(netnetx)
    new_el = []
    for item in cy['elements']['edges']:
        data = {}
        data['data'] = {'id': f"{labels[item['data']['source']]}-{labels[item['data']['target']]}",
        'source': labels[item['data']['source']], 
        'target': labels[item['data']['target']]}
        if item['data']['label'] == 'r':
            data['classes'] = 'rep'
        elif item['data']['label'] == 'a':
            data['classes'] = 'act'
        new_el.append(data)
    new_nl = []
    for item in cy['elements']['nodes']:
        data = {}
        data['data'] = {'label': item['data']['label'], 'id': item['data']['label'], 'name': item['data']['label']}
        new_nl.append(data)
    '''
    the above conversion missing nodes with no in edges which then causes it to miss out edges from these nodes
    Example:
    FKH1 :  : E
    HCM1 : (SWI4) : E
    PLM2 : (SWI4) : E
    SWI4 : (~YHP1) : E
    SWI5 : (FKH1) : E
    TOS4 : (SWI4) : E
    YHP1 :  : E

    FKH1 and YHP1 will not be in cytoscape network. Also, edges [SWI4 : (~YHP1) : E] and [SWI5 : (FKH1) : E] will be missing, too.
    The below corrects for this.
    '''
    # print(netspec.split('\n'))
    # for edge in netspec.split('\n'):
    #     if '(' not in edge and ')' not in edge:
    #         print(edge)
    # print(new_el)
    new_elements = new_nl + new_el
    cyto_styles = [{'selector': 'node', 'style': {'content': 'data(label)'}},
    {'selector': 'edge', 'style': {'curve-style': 'bezier'}},
    {'selector': '.rep', 'style': {'target-arrow-color': 'red', 'line-color': 'red', 'target-arrow-shape': 'tee'}},
    {'selector': '.act', 'style': {'target-arrow-color': 'green', 'target-arrow-shape': 'triangle', 'line-color': 'green'}}]
    return new_elements, cyto_styles

def df_edges_to_cytoscape(data_frame_rows,derived_virtual_selected_rows):
    cyto_elements = []
    for i in list(derived_virtual_selected_rows):
            target = data_frame_rows.iloc[i,0].split("=")[0]
            source = data_frame_rows.iloc[i,0].split("=")[1].split("(")[1].split(")")[0]
            type_reg = data_frame_rows.iloc[i,0].split("=")[1].split("(")[0]

            cyto_elements.append({'data': {'id': source, 'label': source}})
            cyto_elements.append({'data': {'id': target, 'label': target}})
            if type_reg == "tf_rep":
                cyto_elements.append({'data': {'id': f'{source}-{target}', 'source': source, 'target': target}, 'classes': 'rep'})

            else:
                cyto_elements.append({'data': {'id': f'{source}-{target}', 'source': source, 'target': target}, 'classes': 'act'})

    cyto_elements = [i for n, i in enumerate(cyto_elements) if i not in cyto_elements[n + 1:]]
    cyto_styles = [{'selector': 'node', 'style': {'content': 'data(label)'}},
        {'selector': 'edge', 'style': {'curve-style': 'bezier'}},
        {'selector': '.rep', 'style': {'target-arrow-color': 'red', 'line-color': 'red', 'target-arrow-shape': 'tee'}},
        {'selector': '.act', 'style': {'target-arrow-color': 'green', 'target-arrow-shape': 'triangle', 'line-color': 'green'}}]
    return cyto_elements, cyto_styles

def df_edges_to_netspec(data_frame_rows,derived_virtual_selected_rows, column_with_edges_title):
    dff_sub = data_frame_rows.iloc[derived_virtual_selected_rows]
    dff_sub = dff_sub.sort_values(by=column_with_edges_title)
    dff_sub["target"] = dff_sub[column_with_edges_title].str.split("=").str[0]
    dff_sub["edge"] = dff_sub[column_with_edges_title].str.split("=").str[1]
    dff_sub["source"]=dff_sub["edge"].str.split("(").str[1].str.split(")").str[0]
  

    string_network = str()

    targets = set(dff_sub["target"])
    sources = set(dff_sub["source"])
    for i in targets:
        str_model = str(i + " : ")
        acts = list()
        reps = list()
        dff_sub_target=dff_sub.loc[dff_sub.target == i]
        dff_acts = dff_sub_target.loc[dff_sub_target["edge"].str.contains("act")]
        dff_reps = dff_sub_target.loc[dff_sub_target["edge"].str.contains("rep")]

        for j in range(0, len(dff_acts.index)):
            source = dff_acts["edge"].iloc[j].split("(")[1].split(")")[0]
            if (j == 0) & (len(dff_acts.index) == 1):
                if len(dff_reps.index) > 0:
                    str_model = str(str_model+"("+source+")")
                elif len(dff_reps.index) == 0:
                    str_model = str(str_model+"("+source+") : E\n")
            elif (j == 0) & (len(dff_acts.index) > 1):
                str_model = str(str_model+"("+source+"+")
            elif (j != 0) & (j < len(dff_acts.index)-1):
                str_model = str(str_model+source+"+")
            elif (j != 0) & (j == len(dff_acts.index)-1):
                if len(dff_reps.index) > 0:
                    str_model = str(str_model+source+")")
                elif len(dff_reps.index) == 0:
                    str_model = str(str_model+source+") : E\n")
        for k in range(0, len(dff_reps.index)):
            source = dff_reps["edge"].iloc[k].split("(")[1].split(")")[0]
            if (k == 0) & (len(dff_reps.index) == 1):
                str_model = str(str_model+"(~"+source+") : E\n")
            elif (k != 0) & (k == len(dff_reps.index)-1):
                str_model = str(str_model+"(~"+source+") : E\n")
            else:
                str_model = str(str_model+"(~"+source+")")
        string_network = str(string_network+str_model)
    sources_without_in_edges = [i for i in sources if i not in targets]
    for node in sources_without_in_edges:
        str_model  = str(node+' : : E\n')
        string_network = str(string_network+str_model)
    return string_network

def format_tex(float_number):
    superscript = str.maketrans("-0123456789", "⁻⁰¹²³⁴⁵⁶⁷⁸⁹")
    exponent = np.floor(np.log10(float_number))
    mantissa = float_number/10**exponent
    mantissa_format = str(mantissa)[0:3]
    # return f'{mantissa_format}x10^{str(exponent).translate(superscript)}'
    return f'{mantissa_format}x10{str(round(exponent)).translate(superscript)}'


def zscore_convert(df):

    z_mat = stats.zscore(df, axis=1)
    z_df = pd.DataFrame(z_mat, index=df.index, columns=df.columns)

    return z_df


def get_bin_placement(df_row, bin_edges):
    max_tp = df_row['First Cycle Peak Expression']
    for idx, (left_edge, right_edge) in enumerate(zip(bin_edges[:-1], bin_edges[1:])):
        bin_idx = idx + 1
        if bin_idx == len(bin_edges) - 1:
            if max_tp >= left_edge:
                return bin_idx
        else:
            if max_tp >= left_edge and max_tp < right_edge:
                return bin_idx



#from Bree --> edge prevalence code. Takes in DSGRN network list and outputs sorted list of (edge, prevalence) tuples. The edges are in LEM format.
def count_all_edges(networks):
    N = len(networks)
    edges = {}
    for spec in networks:
        lines = spec.split("\n")
        while "" in lines:
            lines.remove("")
        nodes = [(line.split(":")[0].replace(" ",""),line.split(":")[1].replace(" ","")) for line in lines]
        for node, inputs in nodes:
            inedges = inputs.replace("(", " ").replace(")", " ").replace("+"," ").split()
            fulledges = [node+"=tf_act("+i+")" if "~" not in i else node+"=tf_rep("+i[1:]+")" for i in inedges]
            for e in fulledges:
                if e not in edges:
                    edges[e] = 1
                else:
                    edges[e] += 1
    edges = {e : c/N for e,c in edges.items()}
    return sorted(edges.items(),key=lambda x : (x[1],x[0]),reverse=True)

def results_text_formater(tabs_list):
    text = [tab.split('_')[0].capitalize() for tab in tabs_list]
    text_len = len(text)
    if text_len == 1:
        return text[0]
    if text_len == 2:
        return ' and '.join(text)
    else:
        return f'{text[0]}, {text[1]} and {text[2]}'


def filter_df_for_tfs(df, annot_file):

    annot_df = pd.read_csv(annot_file, sep='\t', comment='#', index_col=0)
    tf_annot_df = annot_df[(annot_df['tf_act'] == 1) | ((annot_df['tf_rep'] == 1))]
    
    return df[df.index.isin(tf_annot_df.index)]


def reset_network_index(network_df, ep):
    ep_df = network_df[network_df['Epsilon'] == ep]
    reset_df = ep_df.reset_index().reset_index()
    reset_df = reset_df.rename(columns={'level_0': 'Network Index Epsilon'})
    return reset_df


def make_node_and_edge_lists(lem_table_dict, selected_rows):

        lem_table = pd.DataFrame(lem_table_dict)
        lem_table_sub = lem_table.iloc[selected_rows]
        lem_edge_list = lem_table_sub['model'].tolist()
        edges = list()
        nodes = list()
        for edge in lem_edge_list:
            left_side, right_side = edge.split('=')
            nodes.append(left_side)
            nodes.append(right_side[right_side.find('(')+1:right_side.find(')')] )
            if 'tf_act' in edge:
                edges.append(edge.replace('tf_act', 'a'))
            else:
                edges.append(edge.replace('tf_rep', 'r'))
        node_list = '\n'.join(list(set(nodes)))
        edge_list = '\n'.join(edges)
        return node_list, edge_list