import utils.utils as utils
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output

from server import app
from layouts import node_finding_layout, edge_finding_layout, network_finding_layout, results_network_layout
from callbacks.node_finding_callbacks import *
from callbacks.edge_finding_callbacks import *
from callbacks.network_finding_callbacks import *

dataset_arg_dict = utils.parse_dataset_arg()

nav_links = [dbc.NavLink('Home Page', href='/', active='exact')]
for subdir in dataset_arg_dict['tabdirs_list']:
    page_name = ' '.join([s.capitalize() for s in subdir.split('_')])
    nav_links.append(dbc.NavLink(page_name, href=f'/layouts/{subdir}', active='exact'))

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    dbc.Row([
        dbc.Col([
            dbc.Nav(nav_links, pills=True)
        ], width={'size': 4}, id='nav-bar')
    ]),
    html.Div(id='page-content'),
])


@app.callback(
    Output(component_id='page-content', component_property='children'),
    Input(component_id='url', component_property='pathname')
)
def display_page(pathname):
    if pathname == '/':
        return results_network_layout.results_network_layout
    if pathname == '/layouts/node_finding':
        return node_finding_layout.node_finding_layout
    if pathname == '/layouts/edge_finding':
        return edge_finding_layout.edge_finding_layout
    if pathname == '/layouts/network_finding':
        return network_finding_layout.network_finding_layout
    else:
        return dbc.Container(dcc.Markdown(f'''
                #### This is not an available page. Please click on one of the tabs above to access an available page.
                ''', style={'text-align': 'left', 'marginTop': 30}), fluid=True)

if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8050)
