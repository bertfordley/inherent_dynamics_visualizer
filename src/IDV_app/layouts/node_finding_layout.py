import dash_html_components as html
import dash_core_components as dcc
import utils.utils as utils
import dash_bootstrap_components as dbc


node_finding_layout = dbc.Container(html.Div([
    dbc.Row([dbc.Col([
        dcc.Markdown('''
        ##### Select the Node Finding run to be displayed:
        '''),
        dcc.Dropdown(
            id='dataset-dropdown_nf',
            options=utils.get_dropdown_items(utils.parse_dataset_arg()['results_dir'], 'node_finding'),
            placeholder="Select a Node Finding run",
            style={'width': '500px'}),
    ])
    ]),
    dbc.Row([
        dbc.Col([
            html.Div(id='gene_list_input_nf_div'),
            dcc.Store(id='gene_list_memory'),
            html.Div(id='order_radio_div'),
            html.Div(id='timeseries_dropdown_nf_div'),
            html.Div(id='heatmap_text_nf'),
            html.Div(id='heatmap_fig_nf'),
        ], width=4),
        dbc.Col([
            html.Div(id='cycle_bin_div', style={'display': 'inline-block'}),
            html.Div(id='download-gene_list', style={'display': 'inline-block'}),
            html.Div(id='dlxjtk_table_div_nf')],
            width=4),
        dbc.Col([
            html.Div(id='line-graphs_nf'),
            html.Div(id='annot_table_nf'),
            html.Div(id='download-annot_file', style={'display': 'inline-block'}),
        ],  width=4),
    ])
]),
fluid=True)

