import dash_html_components as html
import dash_core_components as dcc
import utils.utils as utils
import dash_bootstrap_components as dbc
import utils.viz_utils as viz


results_network_layout = dbc.Container(
    html.Div([
        dbc.Row([
            dbc.Col([
                # utils.check_for_empty_dirs()
                dcc.Markdown(f'''
                
                #### You have {utils.results_text_formater(utils.parse_dataset_arg()['tabdirs_list'])} Finding results to explore.
                
                The tabs above allow you to explore results for individual steps of the Inherent Dynamics Pipeline.
                
                ##### The Manifest Table
                The table to the right shows how data and results are connected within a single Pipeline run and across Pipeline runs.

                ##### Empty Results Folders
                Items in the **IDP Output** column annotated with **(No Results)** will not show up in their respective IDP results tab as no results could be found in the respective folder.
                A less obivous case of this is if a queries folder is annotated with No Results. If this is the case, it's related network finding results will not show up in the Network Finding tab.

                ''', style={'text-align': 'left', 'marginTop': 30})], width=3),
                # dcc.Markdown(f'''
                
                # #### You have {utils.results_text_formater(utils.parse_dataset_arg()['tabdirs_list'])} Finding results to explore.
                
                # The tabs above allow you to explore results for individual steps of the Inherent Dynamics Pipeline.
                
                # The tree to the right shows how data and results are connected within a single Pipeline run and across Pipeline runs.
                # ''', style={'text-align': 'left', 'marginTop': 30})], width=3),
            dbc.Col([
                dcc.Markdown('''
                ### Your Manifest
                ''', style={'text-align': 'center'}),
                html.Div(viz.make_manifest_table(), id='mani_table_div')
                # dcc.Markdown('''
                # ### Analysis Lineage
                # ''', style={'text-align': 'center'}),
                # html.Div(dcc.Graph(figure=viz.make_results_tree(), config={'staticPlot':True}), id='results_network')
            ], width=6)
        ])
    ]), fluid=True)
