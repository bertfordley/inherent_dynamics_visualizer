from src.IDV_app.utils.viz_utils import make_lem_summary_table
import dash_html_components as html
import dash_core_components as dcc
import utils.utils as utils
import dash_bootstrap_components as dbc
import dash_table
import utils.viz_utils as viz

edge_finding_layout = dbc.Container(html.Div([
    dbc.Row([dbc.Col([
        dcc.Markdown('''
        ##### Select the Edge Finding run to be displayed:
        '''),
        dcc.Dropdown(
            id='dataset-dropdown_ef',
            options=utils.get_dropdown_items(utils.parse_dataset_arg()['results_dir'], 'edge_finding'),
            placeholder="Select a Edge Finding run",
            style={'width': '500px'})
        ])
        ]),
    dcc.Store(id='lem_compiled_table_memory'),
    dbc.Row([
        dbc.Col([
            html.Div(id='timeseries_dropdown_ef_div'),
            html.Div(id='edge_selection_radio_div'),
            html.Div(id='num_edge_selection'),
            html.Div(id='param_sliders')],
            width = 3),
        dbc.Col([
            html.Div(viz.make_lem_summary_table('lem_summary_table_ef'), id='lem_summary_table_div_ef'),
            html.Div(id='edge_list_dl_div_ef')
            ], width=4),
        dbc.Col([
            html.Div(id='network_selection_div_ef'),
            html.Div(id='network_selected_ef'),
            html.Div(id='netspec_dl_div_ef'),
        ],  width=4)

    ])
]),
fluid=True)
