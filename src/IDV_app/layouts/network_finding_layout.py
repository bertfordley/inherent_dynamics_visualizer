import dash_html_components as html
import dash_core_components as dcc
import utils.utils as utils
import dash_bootstrap_components as dbc
import dash_table
import utils.viz_utils as viz

network_finding_layout = dbc.Container(html.Div([
    dbc.Row([
        dbc.Col([
            dcc.Markdown('''
            ##### Select the dataset to be displayed:
            '''),
            dcc.Dropdown(
                id='dataset-dropdown_netf',
                options=utils.get_dropdown_items(utils.parse_dataset_arg()['results_dir'], 'network_finding'),
                placeholder="Select a dataset",
                style={'width': '500px'})], width = 4),
    ]),
    dcc.Store(id='networks_json_memory'),
    dcc.Store(id='filtered_networks_ids_json_memory'),
    dbc.Row([ 
        dbc.Col(id="title1"), 
        ]),
    dbc.Row([
        dbc.Col([
            html.Div(id='seed_net_netf'),
            html.Div(id='seed_net_params_netf')
        ], width=3),
        dbc.Col([
            html.Div([
                html.Div(id='dsgrn_scatters_radio_div_netf'),
                html.Div(id='dsgrn_epsilon_radio_div_netf'),
                html.Div(id='netf_input1'),
                html.Div(id='netf_input2'),
                html.Div(id='filter_button_div')]),
            html.Div(id='dsgrn_scatters_netf'),
            html.Div(id='network_fig_netf')
        ], width=4),
        dbc.Col([
            html.Div(id="edge_prev_title"),
            html.Div(id='download-edge_prev'),
            html.Div(viz.make_edge_prev_table('edge_prevalence_netf'), id='edge_prev_table_div'),
        ],width = 2),
        dbc.Col([
            html.Div(id='network_view_title'),
            html.Div(id='network_view_index_input'),
            html.Div(id='dsgrn_network_view_netf'),
            html.Div(id='netspec_dl_div')
        ], width=2)
    ]),
    dbc.Row(html.Hr(style={'borderWidth': "0.3vh", "width": "100%", "color": "#FEC700"})),
    dbc.Row([
        dbc.Col(id="title2")
        ]),
    dbc.Row([
        dbc.Col([
            html.Div(viz.make_edge_list_table('edge_list_netf'), id='edge_list_table_div'),
            ], width=1.5),
        dbc.Col([
            html.Div(id='test_net_netf'),
            ], width=2),
        dbc.Col([
                html.Div(id='x-axis_selection-netf'),
                html.Div(id='epsilon_selection'),
                html.Div(id='Similarity_scatter-netf')
            ], width=3),
        dbc.Col([
                html.Div(id="edge_prev_title_sim"),
                html.Div(id='download-edge_prev_sim'),
                html.Div(id='edge_prev_sim')
            ],width = 2),
            dbc.Col([
            html.Div(id='sim_network_view_title'),
            html.Div(id='sim_network_view_index_input'),
            html.Div(id='sim_networks_view_netf'),
            html.Div(id='sim_netspec_dl_div'),
            ], width = 2),
    dcc.Store("networks_sim_json_memory")
            ])
]), fluid=True)
