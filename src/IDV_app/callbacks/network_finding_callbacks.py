import os
import dash
import math
import pandas as pd
import utils.utils as utils
import plotly.express as px
import utils.viz_utils as viz
import dash_cytoscape as cyto
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State

from server import app


@app.callback(
    [Output(component_id='seed_net_netf', component_property='children'),
    Output(component_id='seed_net_params_netf', component_property='children'),
    Output(component_id='title1', component_property='children'),
    Output(component_id='title2', component_property='children')],
    [Input(component_id='dataset-dropdown_netf', component_property='value')],
    prevent_initial_call=True
)
def update_seed_figure(data_dir):

    dataset_arg_dict = utils.parse_dataset_arg()
    results_dir = dataset_arg_dict['results_dir']

    netfind_config_file = utils.get_pipeline_step_config(f'{results_dir}', data_dir)
    seed_filepath = utils.find_file(f'{results_dir}/{data_dir}', os.path.basename(netfind_config_file["networkfile"]))
    seed_elements, seed_styles = utils.netspec2CytoElems(seed_filepath, file = True)


    seed_net_children = [dcc.Markdown('''##### Seed network for DSGRN network perturbations'''),
                              cyto.Cytoscape(
                                  layout={'name': 'circle'},
                                  style={'height': '300px'},
                                  elements=seed_elements,
                                  stylesheet=seed_styles,
                                  userZoomingEnabled=False)]

    seed_net_arg_table_children = viz.get_seed_net_args_table_netf(netfind_config_file)
    title1 = html.Div(dcc.Markdown('#### Network Finding Results'), style={"border":"2px black solid", "text-align": "center"})
    title2 = html.Div(dcc.Markdown('#### Similarity Analysis'), style={"border":"2px black solid", "text-align": "center"})

    return seed_net_children, seed_net_arg_table_children, title1, title2


@app.callback(
    [Output(component_id='dsgrn_scatters_radio_div_netf', component_property='children')],
    [Input(component_id='dataset-dropdown_netf', component_property='value')],
    prevent_initial_call=True
)
def add_dsgrn_radio(data_dir):

    dataset_arg_dict = utils.parse_dataset_arg()
    manifest = dataset_arg_dict['manifest']

    queries_subfile = manifest[manifest['run_input'] == f'networks{data_dir.split("_")[2]}']['run_output'].values[0]
    netque_dir = f'network_queries_{queries_subfile.split("queries")[1]}'
    netque_config_file = utils.get_pipeline_step_config(dataset_arg_dict['results_dir'], netque_dir)

    options = list()
    if netque_config_file['stablefc']:
        options.append({'label': 'Stable Full Cycle', 'value': 'stablefc'})
    if netque_config_file['domain']:
        options.append({'label': 'Domain', 'value': 'domain'})

    dsgrn_radio = dcc.RadioItems(
        options=options,
            value=options[0]['value'],
            labelStyle={'display': 'inline-block'},
            inputStyle={"margin-right": "5px"},
            style={"padding": "5px", "margin": "auto"},
            id='dsgrn_radio_netf',
            inputClassName='radio_spacing')

    return [dsgrn_radio]


@app.callback(
    [Output(component_id='networks_json_memory', component_property='data')],
    [Input(component_id='dataset-dropdown_netf', component_property='value'),
    Input(component_id='dsgrn_radio_netf', component_property='value')],
    prevent_initial_call=True)
def store_results(data_dir, find_option):

    dataset_arg_dict = utils.parse_dataset_arg()
    results_dir = dataset_arg_dict['results_dir']
    manifest = dataset_arg_dict['manifest']

    queries_subfile = manifest[manifest['run_input'] == f'networks{data_dir.split("_")[2]}']['run_output'].values[0]
    netque_dir = f'network_queries_{queries_subfile.split("queries")[1]}'
    netque_config_file = utils.get_pipeline_step_config(f'{results_dir}', netque_dir)

    results_df = pd.DataFrame()

    if find_option == 'stablefc':
        results, _ = utils.get_netque_results(
            f'{results_dir}/{os.path.basename(netque_dir)}',
            netque_config_file['stablefc'], netque_config_file['domain'])
        results_df = utils.convert_stablefc_json_to_df(results)

    elif find_option == 'domain':
        _, results = utils.get_netque_results(
            f'{results_dir}/{os.path.basename(netque_dir)}',
            netque_config_file['stablefc'], netque_config_file['domain'])
        results_df = utils.convert_domain_json_to_df(results)
        
    return [results_df.to_dict('records')]


@app.callback(
    [Output(component_id='netf_input1', component_property='children'),
    Output(component_id='netf_input2', component_property='children'),
    Output(component_id='filter_button_div', component_property='children')],
    [Input(component_id='dsgrn_radio_netf', component_property='value')],
    prevent_initial_call=True)
def add_input_fields(option):

    if option is not None:

        filter_button = html.Button('Get Edge Prevalence from Selected Networks', id='min_max_button', n_clicks=0)

        min_option_input = dcc.Input(id='yaxis_min_input', type="number",
                                min=0.0, max=100.0, step='any', style={'width': '10%'})
        max_option_input = dcc.Input(id='yaxis_max_input', type="number",
                                min=0.0, max=100.0, step='any', style={'width': '10%'})
        min_matches_input = dcc.Input(id='xaxis_min_input', type="number",
                                min=0.0, max=100.0, step='any', style={'width': '10%'})
        max_matches_input = dcc.Input(id='xaxis_max_input', type="number",
                                min=0.0, max=100.0, step='any', style={'width': '10%'})
        if option == 'stablefc':
            return [['% Stable Full Cycle: Min ', min_option_input, 'Max ', max_option_input], 
                    [ '% Matches: Min ', min_matches_input, 'Max ', max_matches_input], 
                    filter_button]
        elif option == 'domain':
            return [['Matches: Min ', min_option_input, 'Max ', max_option_input], 
                    [ '% Matches: Min ', min_matches_input, 'Max ', max_matches_input],
                    filter_button]
    else:
        return [None, None, None]


@app.callback(
    Output(component_id='dsgrn_scatters_netf', component_property='children'),
    [Input(component_id='networks_json_memory', component_property='data'),
     Input(component_id='dsgrn_radio_netf', component_property='value'),
     Input(component_id='ep_radio_netf', component_property='value'),
     Input(component_id='filtered_networks_ids_json_memory', component_property='data')],
    prevent_initial_call=True
)
def make_initial_scatters(data_dict, option, ep, selected_data):

    results_df = pd.DataFrame.from_dict(data_dict)
    if option == 'stablefc':
        scatter_fig = viz.dsgrn_scatter_epsilons(results_df, '% Matches', '% Stable Full Cycle',
                                                    ep, option, f'Epsilon: {ep}', selected_data)
        return dcc.Graph(figure=scatter_fig, id='ep-scatter', config={'modeBarButtonsToRemove': ['lasso2d']})
    elif option == 'domain':
        scatter_fig = viz.dsgrn_scatter_epsilons(results_df, 'Matches', 'Parameter Graph Size',
                                                    ep, option, f'Epsilon: {ep}', selected_data)
        return dcc.Graph(figure=scatter_fig, id='ep-scatter', config={'modeBarButtonsToRemove': ['lasso2d']})


@app.callback(
    Output(component_id='dsgrn_epsilon_radio_div_netf', component_property='children'),
    [Input(component_id='dsgrn_radio_netf', component_property='value'),
    Input(component_id='dataset-dropdown_netf', component_property='value')],
    prevent_initial_call=True
)
def make_epsilon_radios(dsgrn_radio, data_dir):

    if dsgrn_radio is not None:

        dataset_arg_dict = utils.parse_dataset_arg()
        results_dir = dataset_arg_dict['results_dir']
        manifest = dataset_arg_dict['manifest']

        queries_subfile = manifest[manifest['run_input'] == f'networks{data_dir.split("_")[2]}']['run_output'].values[0]
        netque_dir = f'network_queries_{queries_subfile.split("queries")[1]}'
        netque_config_file = utils.get_pipeline_step_config(f'{results_dir}', netque_dir)
        ep_radio = dcc.RadioItems(
            options=[{'label': ep, 'value': ep} for ep in netque_config_file['epsilons']],
            value=netque_config_file['epsilons'][0],
            labelStyle={'display': 'inline-block'},
            inputStyle={"margin-right": "5px"},
            style={"padding": "5px", "margin": "auto"},
            id='ep_radio_netf',
            inputClassName='radio_spacing')

        return ["Epsilon: ", ep_radio]
    else:
        return [None]

@app.callback(
    [Output(component_id='xaxis_min_input', component_property='value'),
    Output(component_id='xaxis_max_input', component_property='value'),
    Output(component_id='yaxis_min_input', component_property='value'),
    Output(component_id='yaxis_max_input', component_property='value')],
    [Input(component_id='ep-scatter', component_property='selectedData'),
    Input(component_id='networks_json_memory', component_property='data'),
    Input(component_id='filtered_networks_ids_json_memory', component_property='data')],
    [State(component_id='xaxis_min_input', component_property='value'),
    State(component_id='xaxis_max_input', component_property='value'),
    State(component_id='yaxis_min_input', component_property='value'),
    State(component_id='yaxis_max_input', component_property='value')],
    prevent_initial_call=True
)
def filter_networks_with_selection(selected_data, network_mem, filtered_mem, xmin_state, xmax_state, ymin_state, ymax_state):
    xaxis_list = list()
    yaxis_list = list()
    if selected_data is not None:
        if selected_data['points'] is not None:
            for data in selected_data['points']:
                xaxis_list.append(data['customdata'][2])
                yaxis_list.append(data['customdata'][3])
            xaxis_min = round(min(xaxis_list) * 100, 2)
            xaxis_max = round(max(xaxis_list) * 100, 2)
            yaxis_min = round(min(yaxis_list) * 100, 2)
            yaxis_max = round(max(yaxis_list) * 100, 2)
            return [xaxis_min, xaxis_max, yaxis_min, yaxis_max]
    elif selected_data is None:
        return [xmin_state, xmax_state, ymin_state, ymax_state]
    else:
        return [None, None, None, None]


@app.callback(
    Output(component_id='filtered_networks_ids_json_memory', component_property='data'),
    [Input(component_id='ep-scatter', component_property='selectedData'),
    Input(component_id='dsgrn_radio_netf', component_property='value'),
    Input(component_id='networks_json_memory', component_property='data'),
    Input(component_id='min_max_button', component_property='n_clicks'),
    Input(component_id='ep_radio_netf', component_property='value')],
    [State(component_id='xaxis_min_input', component_property='value'),
    State(component_id='xaxis_max_input', component_property='value'),
    State(component_id='yaxis_min_input', component_property='value'),
    State(component_id='yaxis_max_input', component_property='value')],
    prevent_initial_call=True
)
def filter_networks_with_input(selectedData, option, data_dict, nclicks, ep, xaxis_min, xaxis_max, yaxis_min, yaxis_max):
    
    last_trigger = dash.callback_context.triggered[0]

    selected_points = list()
    filtered_points = list()

    if selectedData is not None:
        selected_points = [p['pointIndex'] for p in selectedData['points']]

    elif 'min_max_button.n_clicks' in last_trigger['prop_id']:
        if xaxis_min is None:
            return None
        if xaxis_max is None:
            return None
        if yaxis_min is None:
            return None
        if yaxis_max is None:
            return None
        results_df = pd.DataFrame.from_dict(data_dict)
        results_df = results_df[results_df['Epsilon'] == ep]
        if option == 'stablefc':
            y_filter = results_df[results_df['% Stable Full Cycle'] >= math.floor(yaxis_min)/100]
            y_filter = y_filter[y_filter['% Stable Full Cycle'] <= math.ceil(yaxis_max)/100]
        elif option == 'domain':
            y_filter = results_df[results_df['Matches'] >= math.floor(yaxis_min)/100]
            y_filter = y_filter[y_filter['Matches'] <= math.ceil(yaxis_max)/100]
        xy_filter = y_filter[y_filter['% Matches'] >= math.floor(xaxis_min)/100]
        xy_filter = xy_filter[xy_filter['% Matches'] <= math.ceil(xaxis_max)/100]
        filtered_points = xy_filter['Epsilon Network Index'].tolist()

    if len(selected_points) > 0:
        return selected_points
    elif len(filtered_points) > 0:
        return filtered_points


@app.callback(
    [Output(component_id='edge_prevalence_netf', component_property='data'),
    Output(component_id='edge_prevalence_netf', component_property='columns'),
    Output(component_id='edge_prev_title', component_property='children'),
    Output(component_id='download-edge_prev', component_property='children')],
    [Input(component_id='filtered_networks_ids_json_memory', component_property='data'),
    Input(component_id='networks_json_memory', component_property='data'),
    Input(component_id='min_max_button', component_property='n_clicks')],
    prevent_initial_call=True
)

def make_dsgrn_edge_prevalence_table(selection_list, data_dict, nclicks):

    last_trigger = dash.callback_context.triggered

    if 'min_max_button.n_clicks' in last_trigger[0]['prop_id']:
        results_df = pd.DataFrame.from_dict(data_dict)
        selected_networks = results_df[results_df['Epsilon Network Index'].isin(selection_list)]['Network Toplogy'].tolist()

        edge_prev = utils.count_all_edges(selected_networks)
        edge_prev_df = pd.DataFrame(edge_prev, columns = ["model", "prevalence"])
        edge_prev_df["prevalence"]=edge_prev_df["prevalence"].round(3)
        edge_prev_df['id'] = edge_prev_df['model']
        edge_prev_df.set_index('id', inplace=True, drop=True)

        edge_prev_title = dcc.Markdown('##### Edge Prevalence Table')
        return [edge_prev_df.to_dict('records'), [{"name": i, "id": i} for i in edge_prev_df.columns.values], 
        edge_prev_title,
        [html.Button("Download Table", id="btn-edge-prev-file", className='node-btn'), dcc.Download(id="edge-prev-download")]]
    else:
        return [None, None, None, None]

@app.callback(
    Output(component_id='network_view_title', component_property='children'),
    [Input(component_id='filtered_networks_ids_json_memory', component_property='data'),
     Input(component_id='networks_json_memory', component_property='data'),
     Input(component_id='min_max_button', component_property='n_clicks'),
     Input(component_id='dataset-dropdown_netf', component_property='value')],
    prevent_initial_call=True
)
def make_dsgrn_network_view_title(selection_list, data_dict, nclicks, data_dir):

    last_trigger = dash.callback_context.triggered[0]

    if 'min_max_button.n_clicks' in last_trigger['prop_id']:

        dataset_arg_dict = utils.parse_dataset_arg()
        results_dir = dataset_arg_dict['results_dir']
        manifest = dataset_arg_dict['manifest']

        queries_subfile = manifest[manifest['run_input'] == f'networks{data_dir.split("_")[2]}']['run_output'].values[0]
        netque_dir = f'network_queries_{queries_subfile.split("queries")[1]}'
        netque_config_file = utils.get_pipeline_step_config(f'{results_dir}', netque_dir)

        results_df = pd.DataFrame.from_dict(data_dict)
        selected_networks = results_df[results_df['Network Index'].isin(selection_list)]['Network Toplogy'].tolist()

        network_view_title = [dcc.Markdown(f'''
                                            ##### Selected DSGRN Predicted Networks

                                            Selected {len(set(selected_networks))} out of {int(len(results_df)/len(netque_config_file['epsilons']))} Networks

                                            ''')]

        return network_view_title
    else:
        return None


@app.callback(
    Output(component_id='network_view_index_input', component_property='children'),
    [Input(component_id='filtered_networks_ids_json_memory', component_property='data'),
    Input(component_id='min_max_button', component_property='n_clicks')],
    prevent_initial_call=True
)
def make_dsgrn_network_view_input(selection_list, nclicks):
    
    last_trigger = dash.callback_context.triggered[0]

    if 'min_max_button.n_clicks' in last_trigger['prop_id']:
        selection_list = selection_list
        network_idx_input = dcc.Input(id='newtork_idx_view', type="number", value=1.0,
                                    min=1.0, max=len(selection_list), step=1.0, style={'width': '20%'})

        return ['Network Index: ', network_idx_input]
    else:
        return None

@app.callback(
    [Output(component_id='dsgrn_network_view_netf', component_property='children'),
    Output(component_id='netspec_dl_div', component_property='children')],
    [Input(component_id='filtered_networks_ids_json_memory', component_property='data'),
     Input(component_id='networks_json_memory', component_property='data'),
     Input(component_id='newtork_idx_view', component_property='value'),
     Input(component_id='dsgrn_radio_netf', component_property='value'),
     Input(component_id='min_max_button', component_property='n_clicks')],
    prevent_initial_call=True
)
def make_dsgrn_networks_from_scatter_selection(selection_list, data_dict, selected_network_idx, option, nclicks):
    
    last_trigger = dash.callback_context.triggered[0]

    if 'min_max_button.n_clicks' in last_trigger['prop_id'] or 'newtork_idx_view.value' in last_trigger['prop_id'] or 'filtered_networks_ids_json_memory.data' not in last_trigger['prop_id']:

        selected_network = selection_list[selected_network_idx-1]
        results_df = pd.DataFrame.from_dict(data_dict)
        selected_network_topo = results_df[results_df['Epsilon Network Index'] == selected_network]['Network Toplogy'].tolist()[0]

        single_net_all_ep_results = results_df[results_df['Network Toplogy'] == selected_network_topo]

        single_net_datatable = viz.make_single_net_table_netf(single_net_all_ep_results, option, selected_network)
        single_net_elements, single_net_styles = utils.netspec2CytoElems(selected_network_topo)
        single_net_div = html.Div([single_net_datatable,
                                    cyto.Cytoscape(
                                        layout={'name': 'circle'},
                                        style={'height': '400px'},
                                        id='netpec_input_idx',
                                        elements=single_net_elements,
                                        stylesheet=single_net_styles,
                                        userZoomingEnabled=False)], 
                                        style={'display': 'inline-block',
                                                    "width": "100%",
                                                    "border": "2px black solid"})

        return [single_net_div, [html.Button("Download DSGRN NetSpec", id="btn-netspec", className='node-btn'), dcc.Download(id="netspec-download")]]
    else:
        return [None, None]

@app.callback(
    Output(component_id='netspec-download', component_property='data'),
    [Input(component_id='filtered_networks_ids_json_memory', component_property='data'),
    Input(component_id='networks_json_memory', component_property='data'),
    Input(component_id='newtork_idx_view', component_property='value'),
    Input(component_id='btn-netspec', component_property='n_clicks')],
    prevent_initial_call=True
)
def download_netspec_button_click(selection_list, data_dict, selected_network_idx, n_clicks):

    selected_network = selection_list[selected_network_idx-1]

    results_df = pd.DataFrame.from_dict(data_dict)
    selected_network_topo = results_df[results_df['Epsilon Network Index'] == selected_network]['Network Toplogy'].tolist()[0]

    last_elem_touched = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if last_elem_touched == 'btn-netspec.n_clicks':
        return dict(content=selected_network_topo, filename="netspec.txt")


@app.callback(
    [Output(component_id='edge_list_netf', component_property='data'),
    Output(component_id='edge_list_netf', component_property='columns'),
    Output(component_id='x-axis_selection-netf', component_property='children'),
    Output(component_id='epsilon_selection', component_property='children')],
    Input(component_id='dataset-dropdown_netf', component_property='value'),
    prevent_initial_call=True
    )
def make_edge_list_table_netf(data_dir):

    dataset_arg_dict = utils.parse_dataset_arg()
    results_dir = dataset_arg_dict['results_dir']
    manifest = dataset_arg_dict['manifest']
    subdirs_dict = dataset_arg_dict['subdirs_dict']

    netfind_config_file = utils.get_pipeline_step_config(f'{results_dir}', data_dir)
    queries_subfile = manifest[manifest['run_input'] == f'networks{data_dir.split("_")[2]}']['run_output'].values[0]
    netque_dir = f'network_queries_{queries_subfile.split("queries")[1]}'
    netque_config_file = utils.get_pipeline_step_config(f'{results_dir}', netque_dir)
    edge_list_filepath = utils.find_file(f'{results_dir}/{data_dir}', os.path.basename(netfind_config_file["edgefile"]))

    edge_table = pd.read_csv(edge_list_filepath, names = ["edge_list"])
    edge_table['edge_list'] = edge_table['edge_list'].str.replace('r','tf_rep')
    edge_table['edge_list'] = edge_table['edge_list'].str.replace('a','tf_act')
    edge_table['id'] = edge_table['edge_list']
    edge_table.set_index('id', inplace=True, drop=True)

    if 'seed_network' in subdirs_dict:

        if 'networkfile' in netfind_config_file.keys():
            seed_net_file = os.path.basename(netfind_config_file['networkfile'])
        else:
            seed_net_file = os.path.basename(netfind_config_file['seed_net_file'])

        if seed_net_file in os.listdir(f'{results_dir}/input_files'):
            seed_filepath = f'{results_dir}/input_files/{seed_net_file}'
        else:
            try:
                seed_filepath = netfind_config_file['networkfile']
            except:
                seed_filepath = netfind_config_file['seed_net_file']

        edge_model_list = utils.find_seed_network_edge_models_from_file(seed_filepath)
        seed_network_df = pd.DataFrame(edge_model_list, columns = ["edge_list"])
        seed_network_df["id"]=seed_network_df["edge_list"]
        seed_network_df.set_index('id', inplace=True, drop=True)
        edge_table = edge_table.append(seed_network_df)

    options = [{'label': 'Matches', 'value': 'matches'}]
    if netque_config_file['stablefc']:
        options.append({'label': 'Stable Full Cycle', 'value': 'SFC'})
    if netque_config_file['domain']:
        options.append({'label': 'Domain', 'value': 'domain'})

    x_axis_selection = dcc.RadioItems(
        options=options, 
        value=options[0]['value'],
        labelStyle={'display': 'inline-block'},
        inputStyle={"margin-right": "5px"},
        style={"padding": "5px", "margin": "auto"},
        id='x_axis_radio',
        inputClassName='radio_spacing')

 
    epsilon_selection = dcc.RadioItems(
        options=[{'label':ep, 'value':ep} for ep in netque_config_file['epsilons']],
        value=netque_config_file['epsilons'][0],
        labelStyle={'display': 'inline-block'},
        inputStyle={"margin-right": "5px"},
        style={"padding": "5px", "margin": "auto"},
        id='epsilon_radio',
        inputClassName='radio_spacing')
    
    return [edge_table.to_dict('records'), 
            [{"name": i, "id": i} for i in edge_table.columns.values], 
            x_axis_selection, 
            ['Epsilon: ', epsilon_selection]]

@app.callback(
    [Output(component_id='test_net_netf', component_property='children')],
    [Input(component_id='edge_list_netf', component_property='derived_virtual_data'),
    Input(component_id='edge_list_netf', component_property='selected_rows'),
    Input(component_id='dataset-dropdown_netf', component_property='value')],
    prevent_initial_call=True
)

def make_test_network_from_table(rows, derived_virtual_selected_rows, data_dir):
    if derived_virtual_selected_rows is None:
        derived_virtual_selected_rows = []
        return None
    else:
        dff = pd.DataFrame(rows)
        cyto_elements,cyto_styles = utils.df_edges_to_cytoscape(dff, derived_virtual_selected_rows)
        if (data_dir != None) & (derived_virtual_selected_rows!=[]):
                net_children = [html.Div([dcc.Markdown('''#####      Network or Motif for Similarity Analysis'''),cyto.Cytoscape(
                                          layout={'name': 'circle'},
                                          style={'height': '300px'},
                                          elements=cyto_elements,
                                          userZoomingEnabled=False,
                                          stylesheet = cyto_styles
                                          ),
                                          html.Button(id='submit-button-netf', n_clicks=0, children='Submit')

                                      ])]
        elif (data_dir != None) & (derived_virtual_selected_rows == []):
                        net_children = [html.Div([dcc.Markdown('''#####      Network or Motif for Similarity Analysis'''),cyto.Cytoscape(
                                                              layout={'name': 'circle'},
                                                              style={'height': '300px'},
                                                              elements=cyto_elements,
                                                              userZoomingEnabled=False,
                                                               stylesheet = cyto_styles
                                                              ),

                                                          ])]
        else:
            net_children = [html.Div([cyto.Cytoscape(
                              layout={'name': 'circle'},
                              style={'height': '300px'},
                              elements=cyto_elements,
                              userZoomingEnabled=False,
                               stylesheet = cyto_styles
                              ),

                          ])]
    return net_children


@app.callback(
    [Output(component_id='Similarity_scatter-netf', component_property='children'),
    Output(component_id='networks_sim_json_memory', component_property='data'),
    Output(component_id='edge_prev_sim', component_property='children')],
    [Input(component_id='x_axis_radio', component_property='value'),
    Input(component_id ='epsilon_radio', component_property='value'),
    Input(component_id ='submit-button-netf', component_property='n_clicks'),
    Input(component_id='edge_list_netf', component_property='derived_virtual_data'),
    Input(component_id='edge_list_netf', component_property='selected_rows'),
    Input(component_id='networks_json_memory', component_property='data'),
    Input(component_id='dataset-dropdown_netf', component_property='value')],
    prevent_initial_call=True
)

def make_similarity_scatter(x_axis_radio_option, epsilon_radio_option, submit_button_click, rows, derived_virtual_selected_rows,data_dict, data_dir):
    
    results_df = pd.DataFrame.from_dict(data_dict)
    dataset_arg_dict = utils.parse_dataset_arg()
    results_dir = dataset_arg_dict['results_dir']
    manifest = dataset_arg_dict['manifest']

    queries_subfile = manifest[manifest['run_input'] == f'networks{data_dir.split("_")[2]}']['run_output'].values[0]
    netque_dir = f'network_queries_{queries_subfile.split("queries")[1]}'
    netque_config_file = utils.get_pipeline_step_config(f'{results_dir}', netque_dir)
    if submit_button_click > 0:
            if derived_virtual_selected_rows is None:
                derived_virtual_selected_rows = []
                return None
            else:
                dff = pd.DataFrame(rows)
                string_network = utils.df_edges_to_netspec(dff, derived_virtual_selected_rows, "edge_list")
                epsilon = float(epsilon_radio_option)
                scatter_fig = viz.make_similarity_scatter(results_dir, netque_config_file, netque_dir, x_axis_radio_option, epsilon, string_network)
                scatterplot = dcc.Graph(figure = scatter_fig, id="similarity_scatterplot", config={'modeBarButtonsToRemove': ['lasso2d']})
    else:
         scatterplot = None

    edge_prev_table = viz.make_edge_prev_table('edge_prevalence_sim_table')

    return [scatterplot, results_df.to_dict('records'), edge_prev_table]


@app.callback(
    [Output(component_id='edge_prevalence_sim_table', component_property='data'),
    Output(component_id='edge_prevalence_sim_table', component_property='columns'),
    Output(component_id='edge_prev_title_sim', component_property='children'),
    Output(component_id='download-edge_prev_sim', component_property='children')],
    [Input(component_id='similarity_scatterplot', component_property='selectedData'),
    Input(component_id ='submit-button-netf', component_property='n_clicks')],
    prevent_initial_call=True
)

def make_similarity_edge_prevalence_table(selected_data, nclicks):
    
    last_trigger = dash.callback_context.triggered[0]
    edge_prev_title = dcc.Markdown('##### Edge Prevalence Table')

    selected_networks = []
    if selected_data != None:
        if selected_data and selected_data['points']:
            selected_networks = [p['customdata'][0] for p in selected_data['points']]

    edge_prev = utils.count_all_edges(selected_networks)
    edge_prev_df = pd.DataFrame(edge_prev, columns = ["model", "prevalence"])
    edge_prev_df["prevalence"]=edge_prev_df["prevalence"].round(3)
    edge_prev_df['id'] = edge_prev_df['model']
    edge_prev_df.set_index('id', inplace=True, drop=True)

    if 'submit-button-netf.n_clicks' in last_trigger['prop_id']:
        return [edge_prev_df.to_dict('records'), [{"name": i, "id": i} for i in edge_prev_df.columns.values],
    edge_prev_title, [None, None]]

    elif 'similarity_scatterplot.selectedData' in last_trigger['prop_id'] and  last_trigger['value'] is None:
        return [edge_prev_df.to_dict('records'), [{"name": i, "id": i} for i in edge_prev_df.columns.values],
    edge_prev_title, [None, None]]

    else:
        return [edge_prev_df.to_dict('records'), [{"name": i, "id": i} for i in edge_prev_df.columns.values],
        edge_prev_title,
            [html.Button("Download Table", id="btn-edge-prev-sim-file", className='node-btn'), dcc.Download(id="edge-prev-sim-download")]]


@app.callback(
    Output(component_id='sim_network_view_title', component_property='children'),
    [Input(component_id='similarity_scatterplot', component_property='selectedData'),
    Input(component_id ='submit-button-netf', component_property='n_clicks'),
    Input(component_id='edge_list_netf', component_property='selected_rows')],
    prevent_initial_call=True
)
def make_sim_network_view_title(selected_data, nclicks, edge_table):

    last_trigger = dash.callback_context.triggered[0]

    if 'similarity_scatterplot.selectedData' in last_trigger['prop_id'] and last_trigger['value'] is not None:
        selected_networks = [p['customdata'][0] for p in selected_data['points']]
        network_graphs = [dcc.Markdown(f'''##### Selected {len(set(selected_networks))} Networks''')]
        return network_graphs
    else:
        return None


@app.callback(
    Output(component_id='sim_network_view_index_input', component_property='children'),
    [Input(component_id='similarity_scatterplot', component_property='selectedData'),
    Input(component_id ='submit-button-netf', component_property='n_clicks')],
    prevent_initial_call=True
)
def make_sim_network_view_input(selected_data, nclicks):

    last_trigger = dash.callback_context.triggered[0]

    if 'similarity_scatterplot.selectedData' in last_trigger['prop_id'] and last_trigger['value'] is not None:

        sim_network_idx_input = dcc.Input(id='sim_newtork_idx_view', type="number", value=1.0,
                                    min=1.0, max=len(selected_data['points']), step=1.0, style={'width': '20%'})
        return ['Network Index: ', sim_network_idx_input]
    else:
        return None

@app.callback(
    [Output(component_id='sim_networks_view_netf', component_property='children'),
    Output(component_id='sim_netspec_dl_div', component_property='children')],
    [Input(component_id='x_axis_radio', component_property='value'),
     Input(component_id='similarity_scatterplot', component_property='selectedData'),
     Input(component_id='sim_newtork_idx_view', component_property='value'),
     Input(component_id='networks_json_memory', component_property='data')],
    prevent_initial_call=True
)
def make_similarity_networks_from_scatter_selection(option, selected_networks_dict, selected_network_idx, data_dict):

    if option == 'SFC':
        option = 'stablefc'
    else:
        option = 'domain'

    selected_networks = [p['pointIndex'] for p in selected_networks_dict['points']]
    selected_network = selected_networks[selected_network_idx-1]
    results_df = pd.DataFrame.from_dict(data_dict)

    selected_network_topo = results_df[results_df['Epsilon Network Index'] == selected_network]['Network Toplogy'].tolist()[0]
    single_net_all_ep_results = results_df[results_df['Network Toplogy'] == selected_network_topo]
    single_net_datatable = viz.make_single_net_table_netf(single_net_all_ep_results, option, selected_network)
    single_net_elements, single_net_styles = utils.netspec2CytoElems(selected_network_topo)
    single_net_div = html.Div([single_net_datatable,
                                cyto.Cytoscape(
                                    layout={'name': 'circle'},
                                    style={'height': '400px'},
                                    id='sim_netpec_input_idx',
                                    elements=single_net_elements,
                                    stylesheet=single_net_styles,
                                    userZoomingEnabled=False)], 
                                      style={'display': 'inline-block',
                                                "width": "100%",
                                                "border": "2px black solid"})


    return [single_net_div, [html.Button("Download DSGRN NetSpec", id="btn-sim-netspec", className='node-btn'), dcc.Download(id="sim-netspec-download")]]


@app.callback(
    Output(component_id='sim-netspec-download', component_property='data'),
    [Input(component_id='similarity_scatterplot', component_property='selectedData'),
    Input(component_id='networks_sim_json_memory', component_property='data'),
    Input(component_id='sim_newtork_idx_view', component_property='value'),
    Input(component_id='btn-sim-netspec', component_property='n_clicks')],
    prevent_initial_call=True
)
def download_sim_netspec_button_click(selected_networks_dict, data_dict, selected_network_idx, n_clicks):

    results_df = pd.DataFrame.from_dict(data_dict)

    selected_networks = [p['pointIndex'] for p in selected_networks_dict['points']]
    selected_network = selected_networks[selected_network_idx-1]
    selected_network_topo = results_df[results_df['Epsilon Network Index'] == selected_network]['Network Toplogy'].tolist()[0]
    last_elem_touched = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if last_elem_touched == 'btn-sim-netspec.n_clicks':
        return dict(content=selected_network_topo, filename="netspec.txt")


@app.callback(
    Output(component_id='edge-prev-download', component_property='data'),
    [Input(component_id='edge_prevalence_netf', component_property='data'),
    Input(component_id='btn-edge-prev-file', component_property='n_clicks')],
    prevent_initial_call=True
)
def download_edge_prev_button_click(edge_prev_dict, n_clicks):

    last_elem_touched = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if last_elem_touched == 'btn-edge-prev-file.n_clicks':
        edge_prev_df = pd.DataFrame.from_dict(edge_prev_dict)
        return dcc.send_data_frame(edge_prev_df.to_csv, filename="edge_prevalence.tsv", index=False, sep='\t')


@app.callback(
    Output(component_id='edge-prev-sim-download', component_property='data'),
    [Input(component_id='edge_prevalence_sim_table', component_property='data'),
    Input(component_id='btn-edge-prev-sim-file', component_property='n_clicks')],
    prevent_initial_call=True
)
def download_sim_edge_prev_button_click(edge_prev_dict, n_clicks):

    last_elem_touched = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if last_elem_touched == 'btn-edge-prev-sim-file.n_clicks':
        edge_prev_df = pd.DataFrame.from_dict(edge_prev_dict)
        return dcc.send_data_frame(edge_prev_df.to_csv, filename="edge_prevalence_similiarity.tsv", index=False, sep='\t')