import os
import glob
import dash
import urllib
import pandas as pd
import utils.utils as utils
import utils.viz_utils as viz
import dash_cytoscape as cyto
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from server import app


@app.callback(
    [Output(component_id='timeseries_dropdown_ef_div', component_property='children'),
    Output(component_id='edge_selection_radio_div', component_property='children')],
    [Input(component_id='dataset-dropdown_ef', component_property='value')],
    prevent_initial_call=True
)
def make_ts_radio_with_text(data_dir):

    dataset_arg_dict  = utils.parse_dataset_arg()
    config_file = utils.get_pipeline_step_config(dataset_arg_dict['results_dir'], data_dir)
    data_filenames = [os.path.split(filename)[1] for filename in config_file['data_files']]
    
    timeseries_dropdown = dcc.Dropdown(
        options=[{'label': file, 'value': file} for file in data_filenames],
        value=data_filenames[0],
        style={'width': '500px'},
        id='timeseries_dropdown_ef')

    edge_selection_radio = dcc.RadioItems(
            options=[
                {'label': 'LEM Summary Table', 'value': 'lem_sum'},
                {'label': 'Top-Line LEM Table', 'value': 'topline_lem'},
                # {'label': 'From Config File', 'value': 'from_config'},
            ], value='lem_sum',
            labelStyle={'display': 'inline-block'},
            inputStyle={"margin-right": "5px"},
            style={"padding": "5px", "margin": "auto"},
            id='lem_table_select',
            inputClassName='radio_spacing')

    return timeseries_dropdown, edge_selection_radio


@app.callback([Output(component_id='lem_compiled_table_memory', component_property='data'),
              Output(component_id='param_sliders', component_property='children'),
              Output(component_id='num_edge_selection', component_property='children'),
              Output(component_id='network_selection_div_ef', component_property='children')],
              [Input(component_id='dataset-dropdown_ef', component_property='value'),
              Input(component_id='timeseries_dropdown_ef', component_property='value')],
              prevent_initial_call=True)

def store_lem_compiled_table(data_dir, ts_data):

    dataset_arg_dict = utils.parse_dataset_arg()
    results_dir = dataset_arg_dict['results_dir']

    edge_config_file = utils.get_pipeline_step_config(results_dir, data_dir)

    data_filenames = [os.path.split(filename)[1] for filename in edge_config_file['data_files']]
    lem_summary_table = pd.read_csv(f'{results_dir}/{data_dir}/summaries/ts{data_filenames.index(ts_data)}/allscores_ts{data_filenames.index(ts_data)}.tsv',
                              comment='#', sep='\t', index_col=0)
    lem_summary_table = lem_summary_table.reset_index()
    lem_summary_table['id'] = lem_summary_table['model']
    lem_summary_table.set_index('id', inplace=True, drop=True)

    target_filenames = glob.glob(f'{results_dir}/{data_dir}/targets/ts{data_filenames.index(ts_data)}/target**.tsv')
    target_concat_table = pd.DataFrame()

    for file in target_filenames:
        df_temp = pd.read_csv(file, comment='#', sep='\t', index_col=0)
        df_temp = df_temp.reset_index()
        df_temp["model"] = file.split("target_")[1].split("_")[0] + "=" + df_temp["model"]
        df_temp["target"] = file.split("target_")[1].split("_")[0]
        df_temp["topline_order"] = df_temp.index
        df_temp['id'] = df_temp['model']
        df_temp.set_index('id', inplace=True, drop=True)
        target_concat_table = pd.concat([target_concat_table, df_temp])

    compiled_lem_table = lem_summary_table.merge(target_concat_table)

    children_param_slider = viz.make_lem_parameter_sliders()

    children_num_edge = html.Div([
            dcc.Markdown("#### Table Adjustments:"),
            dcc.Markdown('The top n edges within allowed parameter bounds will be displayed in the table.'),
            dcc.Markdown("##### Number of Edges:"),
            dcc.Input(id="num_edge_input", type="text", debounce=True)])

    if 'seed_network' in dataset_arg_dict['subdirs_dict']:
        children_network_select =dcc.Dropdown(
                            id='network_selection_ef',
                            options=[
                                  {'label': 'From Seed', 'value': 'seed'},
                                  {'label': 'From Selection', 'value': 'select'}],
                            value="select",
                            style={'width': '500px'})
    else:
        children_network_select =dcc.Dropdown(
                            id='network_selection_ef',
                            options=[
                                  {'label': 'From Selection', 'value': 'select'}],
                            value="select",
                            style={'width': '500px'})


    return compiled_lem_table.to_dict('records'), children_param_slider, children_num_edge, [dcc.Markdown("##### Network:"), children_network_select]


@app.callback(Output(component_id='num_edge_input', component_property='value'),
              Input(component_id='lem_table_select', component_property='value'))

def update_placeholder_edge_selection(table_select):

    if table_select == "topline_lem":
        return '3'
    elif table_select == "lem_sum":
        return '25'
    # elif table_select == 'from_config':
    #     return '10'


@app.callback(
    [Output(component_id='lem_summary_table_ef', component_property='data'),
    Output(component_id='lem_summary_table_ef', component_property='columns'),
    Output(component_id='lem_summary_table_ef', component_property='selected_rows'),
    Output(component_id='param0_label_child', component_property='children'),
    Output(component_id='param1_label_child', component_property='children'),
    Output(component_id='param2_label_child', component_property='children'),
    Output(component_id='param3_label_child', component_property='children'),
    Output(component_id='param4_label_child', component_property='children')],
    [Input(component_id='lem_table_select', component_property='value'),
    Input(component_id='lem_compiled_table_memory', component_property='data'),
    Input(component_id ='num_edge_input', component_property='value'),
    Input(component_id='param_0_range', component_property='value'),
    Input(component_id='param_1_range', component_property='value'),
    Input(component_id='param_2_range', component_property='value'),
    Input(component_id='param_3_range', component_property='value'),
    Input(component_id='param_4_range', component_property='value'),
    Input(component_id='network_selection_ef', component_property='value'),
    Input(component_id='dataset-dropdown_ef', component_property='value')],
    prevent_initial_call=True
)

def make_lem_summary_table_ef(table_select, table_memory,num_edge, param0, param1, param2,param3, param4, network_select, dataset):

    dataset_arg_dict = utils.parse_dataset_arg()
    results_dir = dataset_arg_dict['results_dir']

    if 'seed_network' in dataset_arg_dict['subdirs_dict']:
        seed_net_dir = f'seed_network_{dataset.split("_")[2]}'
        seed_config_file = utils.get_pipeline_step_config(f'{results_dir}', seed_net_dir)
        seed_filepath = f'{results_dir}/{seed_net_dir}/{os.path.basename(seed_config_file["seed_net_file"])}'

    output_table_original= pd.DataFrame.from_dict(table_memory)
    output_table = output_table_original.reset_index()
    output_table[["loss", "norm_loss", "rhs_param_0", "rhs_param_1", "rhs_param_2", "rhs_param_3", "rhs_param_4"]] = output_table[["loss", "norm_loss", "rhs_param_0", "rhs_param_1", "rhs_param_2", "rhs_param_3", "rhs_param_4"]].round(4)
    output_table["prior"] = output_table["prior"].apply(lambda x: utils.format_tex(x))
    output_table = output_table.drop(columns="inv_temp")

    output_table = output_table.loc[(output_table.rhs_param_0<=param0[1]) & (output_table.rhs_param_0>=param0[0])]
    output_table = output_table.loc[(output_table.rhs_param_1<=param1[1]) & (output_table.rhs_param_1>=param1[0])]
    output_table = output_table.loc[(output_table.rhs_param_2<=param2[1]) & (output_table.rhs_param_2>=param2[0])]
    output_table = output_table.loc[(output_table.rhs_param_3<=param3[1]) & (output_table.rhs_param_3>=param3[0])]
    output_table = output_table.loc[(output_table.rhs_param_4<=param4[1]) & (output_table.rhs_param_4>=param4[0])]

    param0_string = str("**rhs_param_0** (γ: basal expression rate) = `"+str(param0)) +"`"
    param0_child = dcc.Markdown(param0_string)
    param1_string = str("**rhs_param_1** (β: degradation rate) = `"+str(param1)) +"`"
    param1_child = dcc.Markdown(param1_string)
    param2_string = str("**rhs_param_2** (α: maximal transcription rate) = `"+str(param2)) +"`"
    param2_child = dcc.Markdown(param2_string)
    param3_string = str("**rhs_param_3** (K: threshold of regulator) = `"+str(param3)) +"`"
    param3_child = dcc.Markdown(param3_string)
    param4_string = str("**rhs_param_4** (n: Hill coefficient) = `"+str(param4)) +"`"
    param4_child = dcc.Markdown(param4_string)
    if num_edge is None:
        num_edge = 25
    else:
        num_edge = int(num_edge)
    if table_select=='lem_sum':
        if network_select == "select":
            output_table = output_table.iloc[0:num_edge, 1:11]
            rows_return = []
        else:
            if 'seed_network' in dataset_arg_dict['subdirs_dict']:
                edge_model_list = utils.find_seed_network_edge_models_from_file(seed_filepath)
                rows_return =list(output_table.loc[output_table['model'].isin(edge_model_list)].index)
                if any(y<num_edge for y in rows_return):
                    rows_return = rows_return
                else:
                    rows_return = []
                output_table['id'] = output_table['model']
                output_table.set_index('id', inplace=True, drop=True)
                output_table = output_table.iloc[0:num_edge, 1:11]
    elif table_select=='topline_lem':
        output_table = output_table.loc[output_table["topline_order"] <num_edge]
        output_table = output_table.sort_values(['target', 'topline_order'], ascending=[True, True])
        output_table['id'] = output_table['model']
        output_table.set_index('id', inplace=True, drop=True)
        output_table = output_table.iloc[:, 1:11]
        rows_return =[]

    return output_table.to_dict('records'), [{"name": i, "id": i} for i in output_table.columns.values], rows_return, param0_child, param1_child, param2_child, param3_child, param4_child


@app.callback(
    [Output(component_id='network_selected_ef', component_property='children'),
    Output(component_id='netspec_dl_div_ef', component_property='children')],
    [Input(component_id='lem_summary_table_ef', component_property='derived_virtual_data'),
    Input(component_id='lem_summary_table_ef', component_property='selected_rows'),
    Input(component_id='lem_table_select', component_property='value'),
    Input(component_id='network_selection_ef', component_property='value'),
    Input(component_id='dataset-dropdown_ef', component_property='value')],
    prevent_initial_call=True
)

def update_network_figure_ef(rows, derived_virtual_selected_rows, table_select, seednet_dropdown_val, dataset):

    if len(derived_virtual_selected_rows) == 0:
        return [None, None]
    else:
        if seednet_dropdown_val == 'seed':
            dataset_arg_dict = utils.parse_dataset_arg()
            results_dir = dataset_arg_dict['results_dir']

            seed_net_dir = f'seed_network_{dataset.split("_")[2]}'
            seed_config_file = utils.get_pipeline_step_config(f'{results_dir}', seed_net_dir)
            seed_filepath = f'{results_dir}/{seed_net_dir}/{os.path.basename(seed_config_file["seed_net_file"])}'
            cyto_elements_seed, cyto_styles = utils.netspec2CytoElems(seed_filepath, file=True)
            cyto_elements_table, _ = utils.df_edges_to_cytoscape(pd.DataFrame(rows), derived_virtual_selected_rows)
            
            if cyto_elements_seed == cyto_elements_table:
                cyto_elements = cyto_elements_seed
            else:
                cyto_elements = cyto_elements_table

        else:
            dff = pd.DataFrame(rows)
            cyto_elements, cyto_styles = utils.df_edges_to_cytoscape(dff, derived_virtual_selected_rows)

        cyto_image = cyto.Cytoscape(layout={'name': 'circle'},
                                        style={'height': '300px'},
                                        elements=cyto_elements,
                                        userZoomingEnabled=False,
                                        stylesheet=cyto_styles)
        dl_children = [html.Button("Download DSGRN NetSpec", id="btn-netspec-ef", className='node-btn'), dcc.Download(id="netspec-download-ef")]
        return [cyto_image, dl_children]


@app.callback(
    Output(component_id='netspec-download-ef', component_property='data'),
    [Input(component_id='lem_summary_table_ef', component_property='derived_virtual_data'),
    Input(component_id='lem_summary_table_ef', component_property='selected_rows'),
    Input(component_id='lem_table_select', component_property='value'),
    Input(component_id='btn-netspec-ef', component_property='n_clicks')],
    prevent_initial_call=True
)
def download_netspec_button_click_ef(rows, derived_virtual_selected_rows, table_select, n_clicks):

    if len(derived_virtual_selected_rows) == 0:
        return None
    else:
        dff = pd.DataFrame(rows)
        string_network = utils.df_edges_to_netspec(dff, derived_virtual_selected_rows, "model")
        last_elem_touched = [p['prop_id'] for p in dash.callback_context.triggered][0]
        if last_elem_touched == 'btn-netspec-ef.n_clicks':
            return dict(content=string_network, filename="netspec.txt")


@app.callback(
    Output(component_id='edge_list_dl_div_ef', component_property='children'),
    [Input(component_id='lem_summary_table_ef', component_property='derived_virtual_data'),
    Input(component_id='lem_summary_table_ef', component_property='selected_rows'),
    Input(component_id='lem_table_select', component_property='value')],
    prevent_initial_call=True
)
def make_dl_button_edge_list(rows, derived_virtual_selected_rows, table_select):

    if len(derived_virtual_selected_rows) == 0:
        return None
    else:
        return [html.Button("Download Node and Edge Lists", id="btn-edge-list-ef", className='node-btn'), dcc.Download(id="edge-list-download-ef"), dcc.Download(id="node-list-download-ef")]


@app.callback(
    [Output(component_id='edge-list-download-ef', component_property='data'),
    Output(component_id='node-list-download-ef', component_property='data')],
    [Input(component_id='lem_summary_table_ef', component_property='derived_virtual_data'),
    Input(component_id='lem_summary_table_ef', component_property='selected_rows'),
    Input(component_id='lem_table_select', component_property='value'),
    Input(component_id='btn-edge-list-ef', component_property='n_clicks')],
    prevent_initial_call=True
)
def download_netspec_button_click_ef(rows, derived_virtual_selected_rows, table_select, n_clicks):

    if len(derived_virtual_selected_rows) == 0:
        return None
    else:
        node_list, edge_list = utils.make_node_and_edge_lists(rows, derived_virtual_selected_rows)
        last_elem_touched = [p['prop_id'] for p in dash.callback_context.triggered][0]
        if last_elem_touched == 'btn-edge-list-ef.n_clicks':
            return [dict(content=edge_list, filename="edge_file.txt"), dict(content=node_list, filename="node_file.txt")]
