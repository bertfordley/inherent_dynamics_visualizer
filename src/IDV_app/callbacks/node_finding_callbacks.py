import os
import urllib
import dash_table
import numpy as np
import pandas as pd
import dash_core_components as dcc
import dash_html_components as html
import utils.utils as utils
import dash_bootstrap_components as dbc
import utils.viz_utils as viz
from dash.dependencies import Input, Output
import dash

from server import app


@app.callback(
    [Output(component_id='timeseries_dropdown_nf_div', component_property='children'),
    Output(component_id='cycle_bin_div', component_property='children'),
    Output(component_id='order_radio_div', component_property='children')],
    [Input(component_id='dataset-dropdown_nf', component_property='value')],
    prevent_initial_call=True
)
def make_ts_radio_with_text(data_dir):

    dataset_arg_dict = utils.parse_dataset_arg()
    config_file = utils.get_pipeline_step_config(dataset_arg_dict['results_dir'], data_dir)
    data_filenames = [os.path.split(filename)[1] for filename in config_file['data_file']]
    
    timeseries_dropdown = dcc.Dropdown(
        options=[{'label': file, 'value': file} for file in data_filenames],
        value=data_filenames[0],
        style={'width': '500px'},
        id='timeseries_dropdown_nf')

    order_radio = dcc.RadioItems(
            options=[
                {'label': 'First Cycle Max. Expression', 'value': 'max'},
                {'label': 'DLxJTK Rank', 'value': 'dlxjtk'},
            ], value='max',
            labelStyle={'display': 'inline-block'},
            inputStyle={"margin-right": "5px"},
            style={"padding": "5px", "margin": "auto"},
            id='heatmap_order_nf',
            inputClassName='radio_spacing')

    order_text_radio = ['Order genes by: ', order_radio]

    cycle_children = [html.P(),
        dcc.Markdown('''Input integer to divide first cycle into bins: 
        '''),
        dcc.Input(id="cycle-bin-input", type="text", placeholder='7', debounce=True)]

    return timeseries_dropdown, cycle_children, order_text_radio


@app.callback(
    Output(component_id='gene_list_input_nf_div', component_property='children'),
    Input(component_id='dataset-dropdown_nf', component_property='value'),
    prevent_initial_call=True
)
def make_gene_list_input(data_dir):

    dataset_arg_dict = utils.parse_dataset_arg()
    gene_list = utils.get_node_list(f'{dataset_arg_dict["results_dir"]}/{data_dir}/gene_list.tsv')

    gene_list_input = dcc.Input(id='gene_list_input', type="number", value=len(gene_list),
                                min=4.0, max=50.0, step=1.0, style={'width': '8%'})

    text_block = dcc.Markdown(f'''
     ##### Gene expression of DLxJTK-ranked genes. Top: 
                            ''', style={'display': 'inline-block'})

    return [html.P(), text_block, gene_list_input]


@app.callback(
    [Output(component_id='heatmap_fig_nf', component_property='children'),
    Output(component_id='gene_list_memory', component_property='data')],
    [Input(component_id='dataset-dropdown_nf', component_property='value'),
    Input(component_id='timeseries_dropdown_nf', component_property='value'),
    Input(component_id='heatmap_order_nf', component_property='value'),
    Input(component_id='gene_list_input', component_property='value')],
    prevent_initial_call=True
)
def update_heatmap_figure(data_dir, ts_data, order, gene_list_length):

    dataset_arg_dict = utils.parse_dataset_arg()
    results_dir = dataset_arg_dict['results_dir']

    config_file = utils.get_pipeline_step_config(results_dir, data_dir)
    gene_list = utils.get_node_list(f'{results_dir}/{data_dir}/gene_list.tsv')

    data_filenames = [os.path.split(filename)[1] for filename in config_file['data_file']]
    ts_df = pd.read_csv(f'{results_dir}/input_files/{ts_data}', comment='#', index_col=0, sep='\t')

    dlxjtk_df = pd.read_csv(f'{results_dir}/{data_dir}/dlxjtk_results.tsv', comment='#', index_col=0, sep='\t')
    dlxjtk_df = dlxjtk_df.sort_values(by=['dlxjtk_score'])

    if len(gene_list) != gene_list_length:
        annot_base = os.path.basename(config_file['annotation_file'])
        dlxjtk_tfs_df = utils.filter_df_for_tfs(dlxjtk_df, f'{results_dir}/input_files/{annot_base}')
        gene_list = dlxjtk_tfs_df.index.tolist()[:gene_list_length]

    periods_list = utils.parse_periods(config_file['periods'])[data_filenames.index(ts_data)]
    periods = [int(p) for p in periods_list.split(', ')]
    avg_per, col_idx = utils.return_period_col_idx(periods, ts_df.columns.tolist())
    title = f'{ts_data}<br>Periods: {periods_list}; Average Period: {avg_per}'
    
    if len(gene_list) <= 5:
        height = len(gene_list)*60
    else:
        height = len(gene_list)*40
    
    if order == 'max':
        # heatmap_fig = viz.heatmap_max_node(ts_df, title, gene_list, col_idx)
        heatmap_fig = viz.heatmap_max_node(ts_df, title, gene_list, col_idx, 500, height)

    elif order == 'dlxjtk':
        dlxjtk_df = dlxjtk_df[dlxjtk_df.index.isin(gene_list)]
        # might fail when reps are involved
        heatmap_fig = viz.heatmap_order_node(ts_df, title, dlxjtk_df.index.tolist(), 500, height)

    heat_children = [
            dcc.Graph(figure=heatmap_fig, id='heatmap',
                      config={'modeBarButtonsToRemove': ['autoScale2d', 'zoomOut2d', 'zoomIn2d', 'pan2d']}),
    ]

    return [heat_children, [gene_list]]


@app.callback(
    [Output(component_id='dlxjtk_table_div_nf', component_property='children'),
    Output(component_id='download-gene_list', component_property='children')],
    [Input(component_id='dataset-dropdown_nf', component_property='value'),
    Input(component_id='cycle-bin-input', component_property='value'),
    Input(component_id='gene_list_memory', component_property='data'),
    Input(component_id='timeseries_dropdown_nf', component_property='value')],
    prevent_initial_call=True
)
def make_table_div_nf(data_dir, cycle_bins, gene_list, ts_data):

    if cycle_bins is None:
        cycle_bins = 7
    else:
        cycle_bins = int(cycle_bins)

    dataset_arg_dict = utils.parse_dataset_arg()
    results_dir = dataset_arg_dict['results_dir']
    gene_list = gene_list[0]

    dlxjtk_df = pd.read_csv(f'{results_dir}/{data_dir}/dlxjtk_results.tsv',
                            comment='#', index_col=0, sep='\t')
    dlxjtk_df = dlxjtk_df[dlxjtk_df.index.isin(gene_list)]
    dlxjtk_df = dlxjtk_df.drop(['dl_reg_pval', 'jtk_per_pval'], axis=1)
    # might fail when reps are involved
    dlxjtk_df = dlxjtk_df.sort_values(by=['dlxjtk_score'])
    dlxjtk_df = dlxjtk_df.applymap(lambda x: utils.format_tex(x))
    dlxjtk_df = dlxjtk_df.reset_index()

    dlxjtk_df = dlxjtk_df.rename(columns={'index': 'Gene',
                                         'dl_reg_pval_norm': 'Normalized DL Regulation p-value',
                                         'jtk_per_pval_norm': 'Normalized JTK Periodicity p-value',
                                         'dlxjtk_score': 'DLxJTK Score'})
    dlxjtk_df['id'] = dlxjtk_df['Gene']
    dlxjtk_df.set_index('id', inplace=True, drop=False)

    # binning within period
    config_file = utils.get_pipeline_step_config(results_dir, data_dir)
    data_filenames = [os.path.split(filename)[1] for filename in config_file['data_file']]
    first_ts_df = pd.read_csv(f'{results_dir}/input_files/{ts_data}', comment='#', index_col=0, sep='\t')

    periods_list = utils.parse_periods(config_file['periods'])[data_filenames.index(ts_data)]
    periods = [int(p) for p in periods_list.split(', ')]
    avg_per, col_idx = utils.return_period_col_idx(periods, first_ts_df.columns.tolist())
    # suggest_bin_numbs = round(avg_per / cycle_bins)
    ts_df_first_periods = first_ts_df.iloc[:, 0:col_idx+1]
    tp_col = [int(col) for col in ts_df_first_periods.columns.tolist()]
    _, bin_edges = np.histogram(tp_col, cycle_bins)

    ts_df_first_periods.loc[:, 'First Cycle Peak Expression'] = pd.to_numeric(ts_df_first_periods.idxmax(axis=1))
    ts_df_first_periods.loc[:, 'First Cycle Bin #'] = ts_df_first_periods.apply(utils.get_bin_placement, bin_edges=bin_edges, axis=1)
    max_cycle_bin_df = ts_df_first_periods[['First Cycle Peak Expression', 'First Cycle Bin #']]
    dlxjtk_df = dlxjtk_df.merge(max_cycle_bin_df, left_on='Gene', right_index=True)

    return [
        dash_table.DataTable(
            id='dlxjtk_table_nf',
            columns=[{"name": i, "id": i} for i in dlxjtk_df.columns if i != 'id'],
            data=dlxjtk_df.to_dict('records'),
            # fill_width=False,
            style_cell={'textAlign': 'left',
                        'whiteSpace': 'normal',
                        'height': 'auto',
                        },
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'
                }
            ],
            style_header={
                'backgroundColor': 'rgb(230, 230, 230)',
                'fontWeight': 'bold'
            },
            row_selectable="multi",
            selected_rows=[],
            sort_action="native"
    ),
    [html.Button("Download Gene List", id="btn-gene-list", className='node-btn'), dcc.Download(id="gl-download")]]


@app.callback(
    Output(component_id='line-graphs_nf', component_property='children'),
    [Input(component_id='dlxjtk_table_nf', component_property='selected_row_ids'),
    Input(component_id='dataset-dropdown_nf', component_property='value'),
    Input(component_id='cycle-bin-input', component_property='value'),
    Input(component_id='timeseries_dropdown_nf', component_property='value')],
    prevent_initial_call=True
)
def update_linegraph(selected_row_ids, data_dir, cycle_bins, ts_data):

    if cycle_bins is None:
        cycle_bins = 7
    else:
        cycle_bins = int(cycle_bins)

    selected_id_set = set(selected_row_ids or [])
    if len(selected_id_set) == 0:
        return None
    else:

        dataset_arg_dict = utils.parse_dataset_arg()
        results_dir = dataset_arg_dict['results_dir']

        config_file = utils.get_pipeline_step_config(results_dir, data_dir)
        data_filenames = [os.path.split(filename)[1] for filename in config_file['data_file']]
        first_ts_df = pd.read_csv(f'{results_dir}/input_files/{ts_data}', comment='#', index_col=0, sep='\t')

        periods_list = utils.parse_periods(config_file['periods'])[data_filenames.index(ts_data)]
        periods = [int(p) for p in periods_list.split(', ')]
        _, col_idx = utils.return_period_col_idx(periods, first_ts_df.columns.tolist())
        # suggest_bin_numbs = round(avg_per / cycle_bins)
        ts_df_first_periods = first_ts_df.iloc[:, 0:col_idx]
        tp_col = [int(col) for col in ts_df_first_periods.columns.tolist()]
        _, bin_edges = np.histogram(tp_col, cycle_bins)

        selected_ts_df = first_ts_df[first_ts_df.index.isin(selected_id_set)]
        z_ts_df = utils.zscore_convert(selected_ts_df)

        line_fig = viz.node_finding_line_graph(z_ts_df, data_dir, 'z-score', 600, 400, bin_edges)

        return dcc.Graph(figure=line_fig, id='line_fig',
                          config={'modeBarButtonsToRemove': ['autoScale2d', 'zoomOut2d', 'zoomIn2d', 'pan2d']})


@app.callback(
    Output(component_id='gl-download', component_property='data'),
    [Input(component_id='dlxjtk_table_nf', component_property='selected_row_ids'),
    Input(component_id='dataset-dropdown_nf', component_property='value'),
    Input(component_id='btn-gene-list', component_property='n_clicks')],
)
def download_genelist_button_click(selected_row_ids, data_dir, n_clicks):


    last_elem_touched = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if last_elem_touched == 'btn-gene-list.n_clicks':

        dataset_arg_dict = utils.parse_dataset_arg()

        config_file = utils.get_pipeline_step_config(dataset_arg_dict['results_dir'], data_dir)

        csv_header_string = f'# Gene list\n# DLxJTK file: {config_file["dlxjtk_output_file"]}\n# annotation file: {config_file["annotation_file"]}\n# DLxJTK cutoff: Manual selection of genes\n""\n'
        gene_string = '\n'.join(selected_row_ids)
        csv_string = csv_header_string + gene_string
        return dict(content=csv_string, filename="gene_list.txt")


@app.callback(
    [Output(component_id='annot_table_nf', component_property='children'),
    Output(component_id='download-annot_file', component_property='children')],
    Input(component_id='dlxjtk_table_nf', component_property='selected_row_ids'),
    prevent_initial_call=True
)
def update_annot_table(selected_row_ids):

    selected_id_set = set(selected_row_ids or [])
    if len(selected_id_set) == 0:
        return [None, None]
    else:
        header_names = ['tf_act', 'tf_rep', 'target']
        annot_data_list = [dict(Gene=i, **{h: 0 for h in header_names}) for i in selected_row_ids]
        annot_table = dash_table.DataTable(
            id='annot-table_editing',
            columns=([{'id': 'Gene', 'name': 'Gene'}] +
                    [{'id': h, 'name': h, 'type': 'numeric'} for h in header_names]),
            data=annot_data_list,
            editable=True,
            style_header={
                'backgroundColor': 'rgb(230, 230, 230)',
                'fontWeight': 'bold'
            },
            tooltip_header={
                'tf_act': ['Set to 1 to annotate gene as a transcriptional activator'],
                'tf_rep': ['Set to 1 to annotate gene as a transcriptional repressor'],
                'target': ['Set to 1 to annotate gene as a target of a TF']},
            # fill_width=False,
            style_cell={'textAlign': 'left',
                        'whiteSpace': 'normal',
                        'height': 'auto',
                        'minWidth': '40px', 'width': '40px', 'maxWidth': '40px'
                        },
        )

    return [[dcc.Markdown('''
        ##### Editable Gene Annotation Table
        '''),
            annot_table], 
            [html.Button("Download Annot. File", id="btn-annot-file", className='node-btn'), dcc.Download(id="af-download")]]


@app.callback(
    Output(component_id='af-download', component_property='data'),
    [Input(component_id='annot-table_editing', component_property='data'),
    Input(component_id='btn-annot-file', component_property='n_clicks')],
    prevent_initial_call=True
)
def download_annotfile_button_click(annot_dict, n_clicks):

    last_elem_touched = [p['prop_id'] for p in dash.callback_context.triggered][0]
    if last_elem_touched == 'btn-annot-file.n_clicks':
        annot_df = pd.DataFrame.from_dict(annot_dict)
        annot_df.iloc[:, 1:] = annot_df.iloc[:, 1:].astype(bool).astype(int)
        return dcc.send_data_frame(annot_df.to_csv, filename="annot.tsv", index=False, sep='\t')
