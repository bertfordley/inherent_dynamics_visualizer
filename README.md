# Inherent Dynamics Visualizer

==============================

A web application for exploring Inherent Dynamics Pipeline results.

![IDV and IDP](static/Fig1_IDV_overview.png)

----
Requirements
------------
1) You will need [Docker](https://docs.docker.com/get-started/overview/). Installation instructions can be found [here](https://docs.docker.com/get-docker/).

2) You will also need a results directory that is generated from using the Inherent Dynamics Pipeline. This results directory should be unaltered. You may change the name of the directory but any changes within the directory will likely cause the IDVisualizer to fail when reading information and data from the directory. The IDPipeline can be found at [https://gitlab.com/biochron/inherent_dynamics_pipeline](https://gitlab.com/biochron/inherent_dynamics_pipeline).

3) Operating Systems: Currently, the IDV has been tested using Linux (Ubuntu) and iOS (Big Sur) systems. The IDV has been tested on Windows 10 using the Windows Subsystem for Linux 2 (WSL2), which allows Windows 10 users to run Linux and the IDV without the need for a different computer, a virtual machine, or a dual-boot setup. IDV does not currently run successfully on native Windows.

----
\*\* **Before any of the following steps, make sure you have started Docker** \*\*

Installation
------------
```
In the terminal, enter the following commands:
    $ git clone https://gitlab.com/bertfordley/inherent_dynamics_visualizer.git
    $ cd inherent_dynamics_visualizer 
    $ ./install.sh
```
View the IDP results from the JoVE article
------------
```
In the terminal and in the inherent_dynamics_visualizer directory, run:
    $ cd test
    $ ./view_test_results.sh

In a web browser, enter http://localhost:8050/ as the URL.
```
Run IDVisualizer App using IDP results
------------
```
In the terminal, run:
    $ ./viz_results.sh <absolute path to IDP results directory>
    
In a web browser, enter http://localhost:8050/ as the URL.
```
Project Organization
------------

    ├── README.md                 <- The README for IDV project.
    ├── install.sh                <- Bash script for building the IDV docker image
    ├── viz_results.sh            <- Bash script for viewing IDP results in the IDV
    ├── test                       
    |   ├── data/two_wts          <- Directory containing IDP results used in the JoVE article
    │   └── view_test_results.sh  <- Bash script for viewing the IDP results used in the JoVE article
    ├── src                       <- Source code for the IDV.
    |   ├── submodules
    |   |   └── dsgrn_utilities   <- DSGRN utility library (https://github.com/breecummins/dsgrn_utilities)
    │   ├── IDV_app           
    │       ├── callbacks         <- Directory containing callback scripts for each page
    │       ├── layouts           <- Directory containing layouts scripts for each page
    │       ├── utils             <- Directory containing utility scripts
    │       ├── index.py          <- File called for running application
    │       └── server.py         <- File initializing application server
    ├── conda_req.yml             <- For creating a conda environment and install dependencies
    ├── setup.py                  <- Makes project pip installable (pip install -e .) so src can be imported
    ├── Dockerfile                <- File for assembling a Docker image for the IDV
    └── LICENSE

Authors
-------
* **Robert C. Moseley**
* **Sophia Campione**

License
-------
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

--------

