# docker pull continuumio/anaconda3
FROM continuumio/anaconda3

# environment
ENV PATH /opt/conda/bin/:/bin/:$PATH

# copy files
WORKDIR /inherent_dynamics_visualizer
COPY ./*.* /inherent_dynamics_visualizer/
COPY ./src/ ./src/
COPY ./test/ ./test/
COPY ./.git ./.git
COPY ./.gitmodules ./.gitmodules

# Initialize submodules
RUN git submodule init
RUN git submodule update

# conda env
RUN conda env create -f conda_req.yml
RUN ["/bin/bash", "-c", "source activate IDVisualizer" ]
ENV PATH /opt/conda/envs/IDVisualizer/bin:$PATH
ENV CONDA_DEFAULT_ENV IDVisualizer

# install dsgrn design, which installs many sub-packages
RUN pip install -e .

EXPOSE 8050