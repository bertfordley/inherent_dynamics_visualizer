#!/bin/bash
# 
# This bash script is for creating a docker container from the IDV image, mounting IDP results, and then running the IDV with the mounted results.
# ** IMPORTANT ** The 'install.sh' script must be run first before using this script.
#
# ARGUMENTS:
#   the absolute path to a directory containing IDP results. Example: if IDP results are stored in a folder name 'IDP_results' then '/Users/johnsmith/Desktop/IDP_results' would be supplied.
#
# Command Example: 
#   ./viz_results.sh Users/johnsmith/Desktop/IDP_results

abs_path=$1

echo "Results path supplied: ${abs_path}"

docker run --rm -p 8050:8050 -v "${abs_path}:/inherent_dynamics_visualizer/Results" idv_docker python src/IDV_app/index.py /inherent_dynamics_visualizer/Results