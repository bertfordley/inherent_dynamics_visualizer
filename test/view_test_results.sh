#!/bin/bash
# 
# This bash script is for creating a docker container from the IDV image, mounting IDP results from the JoVE manuscript, and then running the IDV with the mounted results.
# ** IMPORTANT ** 
#   The 'install.sh' script must be run first before using this script.
#   You must also be in the 'test' folder when calling this script.
#
# Command Example (must be in `test` folder): 
#   ./view_test_results.sh

echo "The folder you are in: $(pwd)"

docker run --rm -p 8050:8050 -v "$(pwd)/data/two_wts:/inherent_dynamics_visualizer/Results" idv_docker python src/IDV_app/index.py /inherent_dynamics_visualizer/Results