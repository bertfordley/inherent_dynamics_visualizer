# Gene list
# DLxJTK file: /Users/robertmoseley/Desktop/Pipeline/inherent_dynamics_visualizer/data/two_wts/node_finding_20210701094312/dlxjtk_results.tsv
# annotation file: /Users/robertmoseley/Desktop/Pipeline/inherent_dynamics_visualizer/data/files/annotation_files/Scerevisiae_annot_symbols_yeasttract+.tsv
# DLxJTK cutoff: Manual selection of genes
""
SWI4
HCM1
YOX1
NDD1
SWI5
YHP1
FKH1